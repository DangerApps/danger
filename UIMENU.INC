{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

{$IFDEF SECTINT}

function NLSMenuItem(ID : String; Command : word) : PMenuItem;
function NewMenuSeparator : PMenuItem;

function NewMenuItem(Caption : String; Command : word; KeyCode : word) : PMenuItem;
function MenuSibling(Prev, Sibling : PMenuItem) : PMenuItem;
function MenuChild(Parent, Child : PMenuItem) : PMenuItem;

procedure MenuRender(Menu : PMenuItem; Popup : boolean);
procedure MenuMove(Menu : PMenuItem; X, Y : integer);

procedure MenuShow(Menu : PMenuItem);
procedure MenuHide(Menu : PMenuItem);
procedure MainMenuBar;

procedure MenuDisable(Item : PMenuItem);
procedure MenuEnable(Item : PMenuItem);
procedure MenuSetEnabled(Item : PMenuItem; Enabled : boolean);

{$ENDIF}

{$IFDEF SECTIMP}

function NewMenuItem(Caption : String; Command : word; KeyCode : word) : PMenuItem;
var
    M : PMenuItem;
begin
    NewMenuItem := nil;
    New(M);
    if not Assigned(M) then exit;
    FillChar(M^, Sizeof(TMenuItem), 0);
    M^.Caption := Caption;
    if Command = cmSeparator then begin
        M^.Separator := True;
        M^.State := msDisabled;
    end;
    M^.Command := Command;
    M^.KeyCode := KeyCode;
    NewMenuItem := M;
end;

function NLSMenuItem(ID : String; Command : word) : PMenuItem;
var
    Caption, Key : String;
    E : integer;
    KeyCode : word;
begin
    KeyCode := kbNone;
    Caption := Trim(RawNLS('MENU.' + ID));
    Key := Trim(RawNLS('MKEY.' + ID));
    if Key <> '' then begin
        Val(Key, KeyCode, E);
        if E <> 0 then begin
            if Length(Key) = 1 then
                KeyCode := Ord(Key[1])
            else
                KeyCode := kbNone;
        end;
    end;
    NLSMenuItem := NewMenuItem(Caption, Command, KeyCode);
end;

function NewMenuSeparator : PMenuItem;
begin
    NewMenuSeparator := NewMenuItem('', cmSeparator, kbNone);
end;

function MenuSibling(Prev, Sibling : PMenuItem) : PMenuItem;
begin
    While Assigned(Prev) and Assigned(Prev^.Next) do
        Prev := Prev^.Next;
    if Assigned(Prev) then begin
        Prev^.Next := Sibling;
        if Assigned(Prev^.Parent) then begin
            { in case it is used to add sibling to a child  :-) }
            Sibling^.AutoHide := True;
            Sibling^.Parent := Prev^.Parent;
        end;
    end;
    Sibling^.Prev := Prev;
    MenuSibling := Sibling;
end;

function MenuChild(Parent, Child : PMenuItem) : PMenuItem;
begin
    if Assigned(Parent) then begin
        Child^.AutoHide := True;
        if Assigned(Parent^.Child) then
            MenuSibling(Parent^.Child, Child)
        else
            Parent^.Child := Child;
    end;
    Child^.Parent := Parent;
    MenuChild := Child;
end;

procedure RenderItem(Item : PMenuItem; X, Y, W, O : integer);
var
    I : byte;
    J : integer;
    P, H : Integer;
    S : String;
begin
    {$IFDEF LOGS}
    Log('Render Menu Item ' + PtrHex(Item));
    {$ENDIF}
    if Item^.Separator then Item^.State := msDisabled;
    with Item^ do begin
        Video^.FreeSprite(Sprite);
        { Only need 4 for Menu Items, But, I don't feel like changing this
          at the moment. Maybe later. }
        Sprite := Video^.NewSprite(W, UI.Render.Height + UI.Settings.Padding.Y * 2, 5, True);
        if not Assigned(Sprite) then exit; { UI is in a bad state now ! }
        for I := 0 to Sprite^.Count - 1 do begin
            Video^.FreeMask(Sprite^.Sprites^[I].BMask); { make um later }
            Sprite^.Sprites^[I].Sequence := I;
            Video^.ImageFill(Sprite^.Sprites^[I].Image, UI.Settings.Menu[I].Background);
            if Item^.Separator and (Item^.Caption = '') then begin
                for J := O to W - O + 1 do
                    Video^.ImagePutPixel(Sprite^.Sprites^[I].Image,
                        J - 1, UI.Settings.Padding.Y, UI.Settings.Menu[I].Foreground);
            end else begin
                Video^.ImagePutText(Sprite^.Sprites^[I].Image,
                    O, UI.Settings.Padding.Y, Caption, UI.Settings.Menu[I].Foreground);
                if Item^.Keycode <> kbNone then begin
                    P := Pos(UCase(Chr(Lo(KeyCode))), UCase(Item^.Caption));
                    if P > 0 then begin
                        Dec(P);
                        Video^.ImagePutText(Sprite^.Sprites^[I].Image,
                            O + Video^.TextWidth(copy(Item^.Caption, 1, P)),
                            UI.Settings.Padding.Y,
                            Caption[P + 1],
                            UI.Settings.Menu[I + 5].Foreground);
                    end else begin
                        S := FormatNLS('COMBO.ALTPLUS', Chr(Lo(KeyCode)) +
                        NLSDelim);
                        H := Video^.TextWidth(S);
                        Video^.ImagePutText(Sprite^.Sprites^[I].Image,
                            Sprite^.Sprites^[I].Image^.Width - O - H,
                            UI.Settings.Padding.Y,
                            S,
                            UI.Settings.Menu[I].Foreground);
                        P := Pos(UCase(Chr(Lo(KeyCode))), UCase(S));
                        if P > 0 then begin
                            Dec(P);
                            Video^.ImagePutText(Sprite^.Sprites^[I].Image,
                                Sprite^.Sprites^[I].Image^.Width - O - H +
                                Video^.TextWidth(copy(S, 1, P)),
                                UI.Settings.Padding.Y,
                                S[P + 1],
                                UI.Settings.Menu[I + 5].Foreground);
                        end;
                    end;
                end;
            end;
            if ImageCompress then Video^.ImageImplode(Sprite^.Sprites^[I].Image);
        end;
        if State = msNone then State := msNormal;
        Sprite^.Index := State;
        Video^.SpriteMove(Sprite, X, Y);
        Sprite^.Kind := slMenuItem + UI.Render.Level;
    end;
end;

function MenuWidth(Menu : PMenuItem) : integer;
var
    W, MaxWidth, P : integer;
    S : String;
begin
    MaxWidth := 0;
    While Assigned(Menu) and Assigned(Menu^.Prev) do Menu := Menu^.Prev;
    While Assigned(Menu) do begin
        S := Menu^.Caption;
        if Menu^.Keycode <> kbNone then begin
            P := Pos(UCase(Chr(Lo(Menu^.KeyCode))), UCase(S));
            if P < 1 then begin
                S := S + ' ' + FormatNLS('COMBO.ALTPLUS',
                Chr(Lo(Menu^.KeyCode)) + NLSDelim);
            end;
        end;
        W := Video^.TextWidth(S);
        if W > MaxWidth then MaxWidth := W;
        Menu := Menu^.Next;
    end;
    MenuWidth :=  MaxWidth + UI.Settings.Padding.X * 2;
end;

function MenuHeight(Menu : PMenuItem) : integer;
var
    I : integer;
begin
    I := 0;
    While Assigned(Menu) and Assigned(Menu^.Prev) do Menu := Menu^.Prev;
    While Assigned(Menu) do begin
        Inc(I);
        Menu := Menu^.Next;
    end;
    MenuHeight := (UI.Render.Font.Font^.Height + UI.Settings.Padding.X * 2) * I;
end;

procedure RenderItems(MainItem : PMenuItem; X, Y : integer; Direction : byte);
var
    W : integer;
    P : PMenuItem;
begin
    Inc(UI.Render.Level);
    if Direction <> 0 then begin
        P := MainItem;
        W := MenuWidth(P);
        while Assigned(P) do begin
            RenderItem(P, X, Y, W, UI.Settings.Padding.X);
            if Assigned(P^.Child) then
                case Direction of
                    1: RenderItems(P^.Child, X + W, Y, 1);
                end;
            if P^.Separator and (P^.Caption = '') then
                Inc(Y, 1 + UI.Settings.Padding.Y * 2)
            else
                Inc(Y, UI.Render.Height + UI.Settings.Padding.Y * 2);
            P := P^.Next;
        end;
    end else begin
        P := MainItem;
        while Assigned(P) do begin
            W := Video^.TextWidth(P^.Caption) + UI.Settings.Spacing;
            RenderItem(P, X, Y, W, UI.Settings.Spacing div 2);
            if Assigned(P^.Child) then
                RenderItems(P^.Child, X, Y + UI.Render.Height + UI.Settings.Padding.Y * 2, 1);
            Inc(X, W);
            P := P^.Next;
        end;
    end;
    Dec(UI.Render.Level);
end;

procedure MenuRender(Menu : PMenuItem; Popup : boolean);
var
    OldFont : TFontSettings;
begin
    GetFontSettings(OldFont);
    FontBestMatch(UI.Settings.Font, UI.Render.Font.Font);
    if not Assigned(UI.Render.Font.Font) then
        UI.Render.Font.Font := OldFont.Font;
    if not Assigned(UI.Render.Font.Font) then Exit; { Another bad choice. }
    UI.Render.Height := UI.Render.Font.Font^.Height;
    SetFontSettings(UI.Render.Font);
    if Popup then
        RenderItems(Menu, 0,0,1)
    else
        RenderItems(Menu, 0,0,0);
    GetFontSettings(OldFont);
end;

procedure MenuMove(Menu : PMenuItem; X, Y : integer);
var
    P : PMenuItem;
    OX, OY : integer;
begin
    if not (assigned(Menu) and Assigned(Menu^.Sprite)) then exit;
    P := Menu;
    OX := X - P^.Sprite^.Area.Left;
    OY := Y - P^.Sprite^.Area.Top;
    While Assigned(P) do begin
        if Assigned(P^.Sprite) then
            Video^.SpriteMove(P^.Sprite, P^.Sprite^.Area.Left + OX, P^.Sprite^.Area.Top + OY);
        if Assigned(P^.Child) and Assigned(P^.Child^.Sprite) then
            MenuMove(P^.Child, P^.Child^.Sprite^.Area.Left + OX, P^.Child^.Sprite^.Area.Top + OY);
        P := P^.Next;
    end;
end;

procedure MenuShow(Menu : PMenuItem);
var
    P : PMenuItem;
begin
    if not (assigned(Menu) and Assigned(Menu^.Sprite)) then exit;
    P := Menu;
    While Assigned(P) do begin
        if Assigned(P^.Sprite) then
            Video^.SpriteShow(P^.Sprite);
        P := P^.Next;
    end;
end;

procedure MenuHide(Menu : PMenuItem);
var
    P : PMenuItem;
begin
    if not (assigned(Menu) and Assigned(Menu^.Sprite)) then exit;
    P := Menu;
    While Assigned(P) do begin
        if Assigned(P^.Sprite) then begin
            if P^.State <> msDisabled then begin
                P^.State := msNormal;
                Video^.SpriteChange(P^.Sprite, P^.State);
            end;
            Video^.SpriteHide(P^.Sprite);
        end;
        if Assigned(P^.Child) then
            MenuHide(P^.Child);
        P := P^.Next;
    end;
end;

procedure MenuDisable(Item : PMenuItem);
begin
    if Item^.State <> msDisabled then begin
        Item^.State := msDisabled;
        if Assigned(Item^.Sprite) then begin
            Video^.SpriteChange(Item^.Sprite, Item^.State);
            Video^.SpriteHide(Item^.Sprite);            { Probably correct}
        end;
        if Assigned(Item^.Child) then
            MenuHide(Item^.Child);
    end;
end;

procedure MenuEnable(Item : PMenuItem);
begin
    if Item^.State = msDisabled then begin
        Item^.State := msNormal;
        if Assigned(Item^.Sprite) then
            Video^.SpriteChange(Item^.Sprite, Item^.State);
    end;
end;

procedure MenuSetEnabled(Item : PMenuItem; Enabled : boolean);
begin
    if Enabled then
        MenuEnable(Item)
    else
        MenuDisable(Item);
end;

procedure MainMenuBar;
begin
    Video^.Region(0, 0, Video^.Width - 1, UI.Render.Height + UI.Settings.Padding.Y * 2 - 1, UI.Settings.Menu[1].Background)
end;

{ These mouse menu functions are very slow... There is a better way to do it.
Basically, just figure out if mouse is over anything visible, then process
it accordingly. I'll get to it someday, maybe.  }
function IsOverMenu(MenuItem : PMenuItem; XY : TPoint) : boolean;
var
    F : boolean;
    P : PMenuItem;
begin
    F := Assigned(MenuItem^.Sprite) and (MenuItem^.Sprite^.Visible) and
        Video^.SpriteOver(MenuItem^.Sprite, XY);
    P := MenuItem^.Child;
    while Assigned(P) and (Not F) do begin
        if Assigned(P^.Child) then
            F := IsOverMenu(P, XY)
        else
        if Assigned(P^.Sprite) and (P^.Sprite^.Visible) then
            F := Video^.SpriteOver(P^.Sprite, XY);
        P := P^.Next;
    end;
    IsOverMenu := F;
end;

function MenuMouseEvent(Menu : PMenuItem; var Event : TEvent) : boolean;
var
    P, H : PMenuItem;
    SO : boolean;
    X : Byte;
    E : TEvent;
begin
    if not assigned(Menu) then exit;
    if (UI.MenuLast = Menu) and Assigned(UI.MenuOver) then begin
        if MouseEvent(Event) and (Event.Kind and evMouse = evMouseMove) and
            Video^.SpriteOver(UI.MenuOver^.Sprite, Event.Position) then begin
                { No change, mouse just moved and is still over same item }
                MenuMouseEvent := True;
                Exit;
            end;
    end;
    UI.MenuLast := Menu;
    UI.MenuOver := nil;
    P := Menu;
    While Assigned(P) do begin
        if Assigned(P^.Sprite) and (P^.Sprite^.Visible) and (P^.State <> msDisabled) then begin
            X := P^.State;
            if MouseEvent(Event) then begin
                if IsOverMenu(P, Event.Position) then begin
                    SO := Video^.SpriteOver(P^.Sprite, Event.Position);
                    if SO then UI.MenuOver := P;
                    if Event.Buttons <> 0 then begin
                        if SO then begin
                            LastMouseBtns := Event.Buttons;
                            X := msClick;
                            if Assigned(P^.Child) then begin
                                MenuShow(P^.Child);
                            end;
                        end;
                    end else begin
                        X := msHover;
                        if SO and (Event.Kind and evMouseRelease = evMouseRelease) then begin
                            Event.Kind := evMouseMove;
                            Event.Movement.X := 0;
                            Event.Movement.Y := 0;
                            PutEvent(Event);
                            Event.Kind := evNull;
                            if (Not Assigned(P^.Child)) then begin
                                x := msNormal;
                                if P^.Command <> cmNone then begin
                                    E.Kind := evCommand;
                                    E.Command := P^.Command;
                                    E.Trigger := LastMouseBtns;
                                    E.Message := Event.Modifiers;
                                    E.Sender := P;
                                    PutEvent(E);
                                end;
                                if Assigned(P^.Parent) then begin
                                    H := P;
                                    While Assigned(H^.Parent) do begin
                                        H := H^.Parent;
                                    end;
                                    H^.State := msNormal;
                                    Video^.SpriteChange(H^.Sprite, H^.State);
                                    MenuHide(H^.Child);
                                end else if P^.AutoHide then begin
                                    H := P;
                                    While Assigned(H^.Prev) do H := H^.Prev;
                                    MenuHide(H);
                                end;
                                UI.MenuOver := nil;
                            end;
                        end;
                    end;
                end else begin
                    X := msNormal;
                    if Assigned(P^.Child) and assigned(P^.Child^.Sprite) and
                    (P^.Child^.Sprite^.Visible) then begin
                         MenuHide(P^.Child);
                    end;
                end;
            end;
            if P^.State <> X then begin
                P^.State := X;
                Video^.SpriteChange(P^.Sprite, P^.State);
            end;
        end else if (Not Assigned(UI.MenuOver)) and (P^.Sprite^.Visible) and (P^.State = msDisabled) then begin
            if Video^.SpriteOver(P^.Sprite, Event.Position) then
                UI.MenuOver := P;
        end;
        if Assigned(P^.Child) and (Not Assigned(UI.MenuOver)) then
            SO := MenuMouseEvent(P^.Child, Event);
        P := P^.Next;
    end;
    MenuMouseEvent := Assigned(UI.MenuOver);
end;

function MenuKeyEvent(Menu : PMenuItem; var Event : TEvent) : boolean;
var
    Handled : boolean;
begin
    Handled := False;
    while Assigned(Menu) and (not Handled) do begin
        if Menu^.State <> msDisabled then begin
            if Assigned(Menu^.Child) then
                if MenuKeyEvent(Menu^.Child, Event) then
                    Handled := True;
            if (Event.Keycode = Menu^.KeyCode) then begin
                Event.Kind := evCommand;
                Event.Command := Menu^.Command;
                Event.Message.L := Event.Shiftcode;
                Event.Message.H := Event.Statuscode;
                Event.Trigger := 0;
                Event.Sender  := Menu;
                PutEvent(Event);
                Event.Kind := evNull;
                Handled := True;
            end;
        end;
        Menu := Menu^.Next;
    end;
    MenuKeyEvent := Handled;
end;

{$ENDIF}