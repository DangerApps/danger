{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

{$I DANGER.DEF}
program Benchmark_Test_Application;
{ DEFINE DEV}
uses Danger;

type
    TImageTests = array [boolean] of record
        Full, Half, Qtr : LongInt;
    end;
    TBench = record
        FullUpdate,
        FullFill,
        HalfFill,
        QtrFill,
        Pixels8, Pixels16, Pixels32, Pixels64, Pixels128
         : longint;
        Img8, Img16, Img32, Img64, Img128 : TImageTests;
        ImgX8, ImgX16, ImgX32, ImgX64, ImgX128 : TImageTests;
        Get8, Get16, Get32, Get64, Get128 : TImageTests;
        Mask8, Mask16, Mask32, Mask64, Mask128 : TImageTests;
        MaskX8, MaskX16, MaskX32, MaskX64, MaskX128 : TImageTests;
        Shift1, Shift4 : TImageTests;
        Chars, Strs : TImageTests;
        GetNull : TImageTests;
    end;

const
    TestSync  : boolean = true;
    TestASync : boolean = true;
    TestUBuf  : boolean = true;

    Cols : word = 22;
    SecsPer : word = 2;
    Duration : word = 0;
    AbortFlag : boolean = false;
    Font : PFont = nil;
    TestStr : string = '"The Quick Fox jumped over the Lazy Dog"';

var
    ModeInfo : record
        Wide, High, Colors : Word;
    end;
var
    Sync, ASync, UBuf : TBench;

function Abort : boolean;
var
    E : TEvent;
begin
    if not AbortFlag then begin
        if GetEvent(E) then begin
            if E.Kind = evKeyPress then
                AbortFlag := True;
        end;
    end;
    Abort := AbortFlag;
end;

function MaxVal(V : LongInt) : String;
begin
    if V = MaxLongInt then
        MaxVal := 'i'
    else
        MaxVal := IntStr(V div SecsPer);
end;

procedure CountVal(N : String; V : LongInt);
begin
    if V = 0 then Exit;
    WriteLn(RPad(N, Cols), MaxVal(V));
end;

procedure CountImgs(N, S : String; I, X, G : TImageTests);
begin
    if I[false].Full = 0 then exit;
    WriteLn(RPad(N + 'x' + N + ' ' + S + ' put', Cols),
        MaxVal(I[false].Full), ' Full, ',
        MaxVal(I[false].Half), ' Half, ',
        MaxVal(I[false].Qtr), ' Qtr');
    if I[true].Full <> 0 then
    WriteLn(RPad('--compressed put', Cols), MaxVal(I[true].Full), ' Full, ',
        MaxVal(I[true].Half), ' Half, ',
        MaxVal(I[true].Qtr), ' Qtr');
    if X[false].Full <> 0 then
    WriteLn(RPad('--mode put', Cols), MaxVal(X[false].Full), ' Full, ',
        MaxVal(X[false].Half), ' Half, ',
        MaxVal(X[false].Qtr), ' Qtr');
    if X[true].Full <> 0 then
    WriteLn(RPad('--compressed mode put', Cols), MaxVal(X[true].Full), ' Full, ',
        MaxVal(X[true].Half), ' Half, ',
        MaxVal(X[true].Qtr), ' Qtr');
    if G[false].Full <> 0 then
    WriteLn(RPad('--image get', Cols), MaxVal(G[false].Full), ' Full, ',
        MaxVal(G[false].Half), ' Half, ',
        MaxVal(G[false].Qtr), ' Qtr');
end;

procedure CountText(S : String; I: TImageTests);
begin
    if I[false].Full = 0 then exit;
    if S[1] <> '-' then
        S := IntStr(Font^.MonoWidth + Font^.Spacing) + 'x' +
        IntStr(Font^.Height) + ' ' + S;
    WriteLn(RPad(S + ' put', Cols),
        MaxVal(I[false].Full), ' Full, ',
        MaxVal(I[false].Half), ' Half, ',
        MaxVal(I[false].Qtr), ' Qtr');
end;

procedure CountShifts(N : String; I : TImageTests);
begin
    if I[false].Full = 0 then exit;
    WriteLn(RPad(N + ' pixel shift UDLR', Cols), MaxVal(I[false].Full), ' Full, ',
        MaxVal(I[false].Half), ' Half, ',
        MaxVal(I[false].Qtr), ' Qtr');
    WriteLn(RPad('--diagonal', Cols), MaxVal(I[true].Full), ' Full, ',
        MaxVal(I[true].Half), ' Half, ',
        MaxVal(I[true].Qtr), ' Qtr');
end;

procedure BenchSpeeds(S : String; Ops : TBench);
begin
    with Ops do begin
        if FullUpdate = 0 then exit;
        WriteLn;
        WriteLn(S);
        CountVal('Video update FPS: ', FullUpdate);
        CountVal('Full Fill: ', FullFill);
        CountVal('Half Fill: ', HalfFill);
        CountVal('Qtr Fill: ', QtrFill);
        if Shift1[false].Full <> 0 then WriteLn;
        CountShifts('1', Shift1);
        CountShifts('4', Shift4);
        if Pixels8 <> 0 then WriteLn;
        CountVal('8x8, Pixels: ', Pixels8);
        CountVal('16x16, Pixels: ', Pixels16);
        CountVal('32x32, 1K Pixels: ', Pixels32);
        CountVal('64x64, 4K Pixels: ', Pixels64);
        CountVal('128x128, 16K Pixels: ', Pixels128);
        if Img8[false].Full <> 0 then WriteLn;
        CountImgs('8', 'image', Img8, ImgX8, Get8);
        CountImgs('16', 'image', Img16, ImgX16, Get16);
        CountImgs('32', 'image', Img32, ImgX32, Get32);
        CountImgs('64', 'image', Img64, ImgX64, Get64);
        CountImgs('128', 'image', Img128, ImgX128, Get128);
        if Mask8[false].Full <> 0 then WriteLn;
        CountImgs('8', 'mask', Mask8, MaskX8, GetNull);
        CountImgs('16', 'mask', Mask16, MaskX16, GetNull);
        CountImgs('32', 'mask', Mask32, MaskX32, GetNull);
        CountImgs('64', 'mask', Mask64, MaskX64, GetNull);
        CountImgs('128', 'mask', Mask128, MaskX128, GetNull);
        if Chars[false].Full <> 0 then WriteLn;
        CountText('chars', Chars);
        CountText('--' + IntStr(Length(TestStr)) + ' char string', Strs);
    end;
end;

procedure BenchReport(Mode : word);
begin
    with Video^ do begin
        WriteLn(RPad('Driver Name:', Cols), Name);
        WriteLn(RPad('Driver Version:', Cols), IntStr(Hi(Version)), '.', ZPad(Lo(Version), 2));
        WriteLn(RPad('Driver Mode: ', Cols), '0x', WordHex(Mode),
            ' (', ModeInfo.Wide, 'x', ModeInfo.High, 'x', ModeInfo.Colors, ')');
    end;
    {$IFNDEF DEV}
    BenchSpeeds('Synchronous (max speed is roughly the video refresh rate) per second', Sync);
    BenchSpeeds('Asynchronous (some shearing, buffered video speed) per second', ASync);
    {$ENDIF}
    BenchSpeeds('Unbuffered (no buffering, raw driver speed) per second', UBuf);
end;

function Elapsed(TimerReset : boolean) : boolean;
const
    X : LongInt = 0;
    C : LongInt = 0;
    S : word = 0;
var
    T, CS : word;
begin
    if TimerReset then begin
        X := TimerTick;
        While X = TimerTick do;
        X := TimerTick;
        SysGetTime(T, T, S, T);
        C := 0;
    end else if X <> TimerTick then begin
        X := TimerTick;
        SysGetTime(T, T, CS, T);
        if CS <> S then begin
            if CS < S then
                Inc(C, (60 + CS - S))
            else
                Inc(C, (CS - S));
            S := CS;
        end;
    end;
    Elapsed := C >= SecsPer;
end;

function ResetTimer : boolean;
begin
    if Abort then begin
        ResetTimer := False;
        exit;
    end else
        ResetTimer := True;
    Inc(Duration, SecsPer);
    Elapsed(True);
end;

procedure IncCount(var I : LongInt);
begin
    if I < MaxLongInt then Inc(I);
end;


function Pattern(Size, Style : word) : PImage;
var
    P : PImage;
    I, X, Y : integer;
    T : boolean;
begin
    P := Video^.NewImage(Size, Size);
    case Style of
        0 : Video^.ImageFill(P, 0);
        3 : begin
            Y := 0;
            while Y < Size do begin
                X := 0;
                T := Y and 1 = 1;
                while X < Size do begin
                    if T then
                        Video^.ImagePutPixel(P, X, Y, Video^.Colors - 1)
                    else
                        Video^.ImagePutPixel(P, X, Y, 0);
                    T := Not T;
                    Inc(X);
                end;
                Inc(Y);
            end;
        end;
        4 : begin
            Video^.ImageFill(P, 0);
            for I := 0 to Size - 1 do begin
                if I and 1 = 0 then begin
                    Video^.ImageRegion(P, 0, I, Size - 1, I, Video^.Colors - 1);
                    Video^.ImageRegion(P, I, 0, I, Size - 1, Video^.Colors - 1);
                end;
            end;
        end;
    else
        for I := 0 to Size div 4 do
        Video^.ImageRegion(P, I, I, Size - I - 1, Size - I - 1, I * 2)
    end;
    Pattern := P;
end;

procedure ScreenFills(var Ops : TBench);
var
    I : integer;
begin
    { Screen Fills }
    I := 0;
    if not ResetTimer then Exit;
    repeat
        Inc(I);
        if I >= Video^.Colors then I := 0;
        Video^.Fill(I);
        Video^.Update;
        IncCount(Ops.FullFill);
    until Elapsed(False);
    if not ResetTimer then Exit;
    repeat
        Inc(I);
        if I >= Video^.Colors then I := 0;
        Video^.Region(0, GetMaxY div 4, GetMaxX, GetMaxY - GetMaxY div 4, I);
        Video^.Update;
        IncCount(Ops.HalfFill);
    until Elapsed(False);
    if not ResetTimer then Exit;
    repeat
        Inc(I);
        if I >= Video^.Colors then I := 0;
        Video^.Region(GetMaxX div 4, GetMaxY div 4,
            GetMaxX - GetMaxX div 4, GetMaxY - GetMaxY div 4, I);
        Video^.Update;
        IncCount(Ops.QtrFill);
    until Elapsed(False);
end;

function PutPixels(Size : word) : longint;
var
    I, X, Y : Integer;
    O : LongInt;
begin
    PutPixels := 0;
    O := 0;
    I := 0;
    Dec(Size);
    if not ResetTimer then Exit;
    repeat
        Inc(I);
        if I >= Video^.Colors then I := 0;
        for Y := 0 to Size  do
            for X := 0 to Size do
                Video^.PutPixel(X, Y, I);
        Video^.Update;
        IncCount(O);
    until Elapsed(False);
    PutPixels := O;
end;

procedure PixelDrawing(var Ops : TBench);
begin
    Ops.Pixels8 := PutPixels(8);
    Ops.Pixels16 := PutPixels(16);
    Ops.Pixels32 := PutPixels(32);
    Ops.Pixels64 := PutPixels(64);
    Ops.Pixels128 := PutPixels(128);
end;

procedure MoveXY(Size, Ofs : word; var X, Y, D : integer);
begin
    case D of
        0 : begin
            Inc(Y, Size);
            if Y > GetMaxY - Size + Ofs then begin
                Y := GetMaxY - Size + Ofs;
                Inc(X, Size);
                D := 1;
            end;
        end;
        1 : begin
            Inc(X, Size);
            if X > GetMaxX - Size + Ofs then begin
                X := GetMaxX - Size + Ofs;
                Dec(Y, Size);
                D := 2;
            end;
        end;
        2 : begin
            Dec(Y, Size);
            if Y < - Ofs then begin
                Y := - Ofs;
                Dec(X, Size);
                D := 3;
            end;
        end;
        3 : begin
            Dec(X, Size);
            if X < - Ofs then begin
                X := - Ofs;
                Inc(Y, Size);
                D := 0;
            end;
        end;
    end;
end;

procedure PutImages(Size : word; var O : TImageTests);
var
    Pass : integer;
    X, Y, I, D : Integer;
    P : PImage;
begin
    P := Pattern(Size, 1);
    for Pass := 0 to 1 do begin
        if Pass = 1 then Video^.ImageImplode(P);
        if not ResetTimer then Exit;
        X := 0;
        Y := 0;
        I := 0;
        repeat
            Video^.PutImage(P, X, Y);
            Inc(X, Size);
            if X + Size > GetMaxX then begin
                X := I and $3;
                Inc(Y, Size);
                if Y + Size > GetMaxY then begin
                    Y := 0;
                    Inc(I);

                end;
            end;
            Video^.Update;
            IncCount(O[Pass = 1].Full);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size div 2;
        Y := X;
        I := 0;
        repeat
            Video^.PutImage(P, X, Y);
            MoveXY(Size, Size div 2, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Half);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size * 3 div 4;
        Y := X;
        I := 0;
        repeat
            Video^.PutImage(P, X, Y);
            MoveXY(Size, Size * 3 div 4, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Qtr);
        until Elapsed(False);
    end;
    Video^.FreeImage(P);
end;

procedure PutModeImages(Size : word; var O : TImageTests);
var
    Pass : integer;
    X, Y, I, D : Integer;
    P : PImage;
begin
    P := Pattern(Size, 2);
    for Pass := 0 to 1 do begin
        if Pass = 1 then Video^.ImageImplode(P);
        if not ResetTimer then Exit;
        X := 0;
        Y := 0;
        I := 0;
        repeat
            Video^.PutImageMode(P, X, Y, imXOR);
            Inc(X, Size);
            if X + Size > GetMaxX then begin
                X := I and $3;
                Inc(Y, Size);
                if Y + Size > GetMaxY then begin
                    Y := 0;
                    Inc(I);

                end;
            end;
            Video^.Update;
            IncCount(O[Pass = 1].Full);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size div 2;
        Y := X;
        I := 0;
        repeat
            Video^.PutImageMode(P, X, Y, imXOR);
            MoveXY(Size, Size div 2, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Half);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size * 3 div 4;
        Y := X;
        I := 0;
        repeat
            Video^.PutImageMode(P, X, Y, imXOR);
            MoveXY(Size, Size * 3 div 4, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Qtr);
        until Elapsed(False);
    end;
    Video^.FreeImage(P);
end;

procedure GetImages(Size, Stage : word; var O : TImageTests);
var
    Pass : integer;
    X, Y, I, D : Integer;
    P : PImage;
begin
    P := Pattern(Size, 1);
    case Stage of
        0: begin
            if not ResetTimer then Exit;
            X := 0;
            Y := 0;
            I := 0;
            repeat
                Video^.GetImage(P, X, Y);
                Inc(X, Size);
                if X + Size > GetMaxX then begin
                    X := I and $3;
                    Inc(Y, Size);
                    if Y + Size > GetMaxY then begin
                        Y := 0;
                        Inc(I);

                    end;
                end;
                Video^.Update;
                IncCount(O[Pass = 1].Full);
            until Elapsed(False);
        end;
        1: begin
            if not ResetTimer then Exit;
            X := -Size div 2;
            Y := X;
            I := 0;
            repeat
                Video^.GetImage(P, X, Y);
                MoveXY(Size, Size div 2, X, Y, I);
                Video^.Update;
                IncCount(O[Pass = 1].Half);
            until Elapsed(False);
        end;
        2 : begin
            if not ResetTimer then Exit;
            X := -Size * 3 div 4;
            Y := X;
            I := 0;
            repeat
                Video^.GetImage(P, X, Y);
                MoveXY(Size, Size * 3 div 4, X, Y, I);
                Video^.Update;
                IncCount(O[Pass = 1].Qtr);
            until Elapsed(False);
        end;
    end;
    Video^.FreeImage(P);
end;

procedure ImageDrawing(var Ops : TBench);
begin
    GetImages(32,0,Ops.Get32);
    PutImages(8, Ops.Img8);
    GetImages(32,1,Ops.Get32);
    PutModeImages(8, Ops.ImgX8);

    GetImages(32,2,Ops.Get32);
    PutImages(16, Ops.Img16);
    GetImages(64,0,Ops.Get64);
    PutModeImages(16, Ops.ImgX16);

    GetImages(64,1,Ops.Get64);
    PutImages(32, Ops.Img32);
    GetImages(64,2,Ops.Get64);
    PutModeImages(32, Ops.ImgX32);

    GetImages(64,0,Ops.Get32);
    PutImages(64, Ops.Img64);
    GetImages(64,1,Ops.Get64);
    PutModeImages(64, Ops.ImgX64);

    GetImages(64,2,Ops.Get32);
    PutImages(128, Ops.Img128);
    GetImages(8,0,Ops.Get8);
    PutModeImages(128, Ops.ImgX128);
end;

procedure PutMasks(Size : word; var O : TImageTests);
var
    Pass : integer;
    X, Y, I, D : Integer;
    P : PImage;
    M : PMask;
begin
    P := Pattern(Size, 3);
    M := Video^.ImageToMask(P, 255);
    for Pass := 0 to 1 do begin
        if Pass = 1 then Video^.MaskImplode(M);
        Video^.Fill(Size + Pass);
        if not ResetTimer then Exit;
        X := 0;
        Y := 0;
        I := 0;
        repeat
            Video^.PutMask(M, X, Y);
            Inc(X, Size);
            if X + Size > GetMaxX then begin
                X := I and $3;
                Inc(Y, Size);
                if Y + Size > GetMaxY then begin
                    Y := 0;
                    Inc(I);

                end;
            end;
            Video^.Update;
            IncCount(O[Pass = 1].Full);
        until Elapsed(False);
        Video^.Fill(Size + Pass + 10);
        if not ResetTimer then Exit;
        X := -Size div 2;
        Y := X;
        I := 0;
        repeat
            Video^.PutMask(M, X, Y);
            MoveXY(Size, Size div 2, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Half);
        until Elapsed(False);
        Video^.Fill(Size + Pass + 20);
        if not ResetTimer then Exit;
        X := -Size * 3 div 4;
        Y := X;
        I := 0;
        repeat
            Video^.PutMask(M, X, Y);
            MoveXY(Size, Size * 3 div 4, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Qtr);
        until Elapsed(False);
    end;
    Video^.FreeMask(M);
    Video^.FreeImage(P);
end;

procedure PutModeMasks(Size : word; var O : TImageTests);
var
    Pass : integer;
    X, Y, I, D : Integer;
    P : PImage;
    M : PMask;
begin
    P := Pattern(Size, 4);
    M := Video^.ImageToMask(P, 0);
    for Pass := 0 to 1 do begin
        if Pass = 1 then Video^.MaskImplode(M);
        if not ResetTimer then Exit;
        X := 0;
        Y := 0;
        I := 0;
        repeat
            Video^.PutMaskMode(M, X, Y, imXOR);
            Inc(X, Size);
            if X + Size > GetMaxX then begin
                X := I and $3;
                Inc(Y, Size);
                if Y + Size > GetMaxY then begin
                    Y := 0;
                    Inc(I);

                end;
            end;
            Video^.Update;
            IncCount(O[Pass = 1].Full);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size div 2;
        Y := X;
        I := 0;
        repeat
            Video^.PutMaskMode(M, X, Y, imXOR);
            MoveXY(Size, Size div 2, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Half);
        until Elapsed(False);
        if not ResetTimer then Exit;
        X := -Size * 3 div 4;
        Y := X;
        I := 0;
        repeat
            Video^.PutMaskMode(M, X, Y, imXOR);
            MoveXY(Size, Size * 3 div 4, X, Y, I);
            Video^.Update;
            IncCount(O[Pass = 1].Qtr);
        until Elapsed(False);
    end;
    Video^.FreeImage(P);
end;


procedure MaskDrawing(var Ops : TBench);
begin
    GetImages(8,1,Ops.Get8);
    PutMasks(8,Ops.Mask8);
    GetImages(8,2,Ops.Get8);
    PutModeMasks(8, Ops.MaskX8);
    GetImages(16,0,Ops.Get16);
    PutMasks(16,Ops.Mask16);
    GetImages(16,1,Ops.Get16);
    PutModeMasks(16, Ops.MaskX16);
    GetImages(16,2,Ops.Get16);
    PutMasks(32,Ops.Mask32);
    GetImages(128,0,Ops.Get128);
    PutModeMasks(32, Ops.MaskX32);
    GetImages(128,1,Ops.Get128);
    PutMasks(64,Ops.Mask64);
    GetImages(128,2,Ops.Get128);
    PutModeMasks(64, Ops.MaskX64);
    PutMasks(128,Ops.Mask128);
    PutModeMasks(128, Ops.MaskX128);
end;


procedure RegionShift(x1, y1, x2, y2 : integer; Pixels : integer; Diag : boolean; var I : longint);
begin
    if not ResetTimer then Exit;
    repeat
        if Elapsed(False) then Break;
        if Diag then
            Video^.ShiftRegion(x1, y1, x2, y2, dmUpRight, Pixels, 0)
        else
            Video^.ShiftRegion(x1, y1, x2, y2, dmUp, Pixels, 0);
        Video^.Update;
        IncCount(I);
        if Elapsed(False) then Break;
        if Diag then
            Video^.ShiftRegion(x1, y1, x2, y2, dmDownRight, Pixels, 0)
        else
            Video^.ShiftRegion(x1, y1, x2, y2, dmRight, Pixels, 0);
        Video^.Update;
        IncCount(I);
        if Elapsed(False) then Break;
        if Diag then
            Video^.ShiftRegion(x1, y1, x2, y2, dmDownLeft, Pixels, 0)
        else
            Video^.ShiftRegion(x1, y1, x2, y2, dmDown, Pixels, 0);
        Video^.Update;
        IncCount(I);
        if Elapsed(False) then Break;
        if Diag then
            Video^.ShiftRegion(x1, y1, x2, y2, dmUpLeft, Pixels, 0)
        else
            Video^.ShiftRegion(x1, y1, x2, y2, dmLeft, Pixels, 0);
        Video^.Update;
        IncCount(I);
    until Elapsed(False);
end;

procedure ScreenShift(Pixels : word; var O : TImageTests);
var
    I : Integer;
begin
    if not ResetTimer then Exit;
    I := 0;
    repeat
        if Elapsed(False) then Break;
        Video^.Shift(dmUp, Pixels, 0);
        Video^.Update;
        IncCount(O[false].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmRight, Pixels, 0);
        Video^.Update;
        IncCount(O[false].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmDown, Pixels, 0);
        Video^.Update;
        IncCount(O[false].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmLeft, Pixels, 0);
        Video^.Update;
        IncCount(O[false].Full);
    until false;
    if not ResetTimer then Exit;
    repeat
        if Elapsed(False) then Break;
        Video^.Shift(dmUpRight, Pixels, 0);
        Video^.Update;
        IncCount(O[true].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmDownRight, Pixels, 0);
        Video^.Update;
        IncCount(O[true].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmDownLeft, Pixels, 0);
        Video^.Update;
        IncCount(O[true].Full);
        if Elapsed(False) then Break;
        Video^.Shift(dmUpLeft, Pixels, 0);
        Video^.Update;
        IncCount(O[true].Full);
    until false;
    RegionShift(0, GetMaxY div 4, GetMaxX - 1, GetMaxY * 3 div 4, Pixels, False, O[false].Half);
    RegionShift(0, GetMaxY div 4, GetMaxX - 1, GetMaxY * 3 div 4, Pixels, True, O[true].Half);
    RegionShift(GetMaxX div 4, GetMaxY div 4,
        GetMaxX * 3 div 4, GetMaxY * 3 div 4, Pixels, False, O[false].Qtr);
    RegionShift(GetMaxX div 4, GetMaxY div 4,
        GetMaxX * 3 div 4, GetMaxY * 3 div 4, Pixels, True, O[true].Qtr);
end;

procedure ScreenShifting(var Ops : TBench);
begin
    ScreenShift(1,Ops.Shift1);
    ScreenShift(4,Ops.Shift4);
end;

procedure MoveXY2(SizeX, SizeY, Factor : integer; var X, Y, D : integer);
begin
    case D of
        0 : begin
            Inc(Y, SizeY);
            if Y > GetMaxY - (SizeY div Factor) then begin
                Y := GetMaxY - (SizeY div Factor);
                Inc(X, SizeX);
                D := 1;
            end;
        end;
        1 : begin
            Inc(X, SizeX);
            if X > GetMaxX - (SizeX div Factor) then begin
                X := GetMaxX - (SizeX div Factor);
                Dec(Y, SizeY);
                D := 2;
            end;
        end;
        2 : begin
            Dec(Y, SizeY);
            if Y < (SizeY div Factor - SizeY) then begin
                Y := (SizeY div Factor - SizeY);
                Dec(X, SizeX);
                D := 3;
            end;
        end;
        3 : begin
            Dec(X, SizeX);
            if X < (SizeX div Factor - SizeX) then begin
                X := (SizeX div Factor - SizeX);
                Inc(Y, SizeY);
                D := 0;
            end;
        end;
    end;
end;


procedure CharTest(var Ops : TImageTests);
var
    CW, CH, X, Y, C, I, D : integer;
begin
    CW := Video^.GetFont^.MonoWidth + Video^.GetFont^.Spacing;
    CH := Video^.GetFont^.Height;
    if not ResetTimer then exit;
    X := 0;
    Y := 0;
    C := 1;
    I := 0;
    repeat
        Inc(Ops[False].Full);
        Video^.PutChar(X, Y, Chr(I), C);
        Video^.Update;
        Inc(I);
        if I > 255 then I := 0;
        Inc(X, CW);
        if X + CW >= GetMaxX then begin
            X := 0;
            Inc(Y, CH);
            if Y + CH >= GetMaxY then begin
                Y := 0;
                I := 0;
                Inc(C);
                if C >= Video^.Colors then
                    C := 0;
            end;
        end;
    until Elapsed(false);
    X := -CW div 2;
    Y := -CH div 2;
    I := 65;
    D := 0;
    if not ResetTimer then exit;
    repeat
        Inc(Ops[False].Half);
        Video^.PutChar(X, Y, Chr(I), C);
        Video^.Update;
        Inc(I);
        if I > 96 then I := 65;
        MoveXY2(CW, CH, 2, X, Y, D);
        if D = 0 then begin
            Inc(C);
            if C >= Video^.Colors then
                C := 0;
        end;
    until Elapsed(false);
    X := (CW div 4) - CW;
    Y := (CH div 4) - CH;
    D := 0;
    if not ResetTimer then exit;
    repeat
        Inc(Ops[False].Qtr);
        Video^.PutChar(X, Y, Chr(I), C);
        Video^.Update;
        Inc(I);
        if I > 96 then I := 65;
        MoveXY2(CW, CH, 4, X, Y, D);
        if D = 0 then begin
            Inc(C);
            if C >= Video^.Colors then
                C := 0;
        end;
    until Elapsed(false);
end;

procedure StrTest(var Ops : TImageTests);
var
    CW, CH, X, Y, C, D : integer;
begin
    CW := Video^.GetFont^.MonoWidth + Video^.GetFont^.Spacing;
    CH := Video^.GetFont^.Height;
    if not ResetTimer then exit;
    X := 0;
    Y := 0;
    C := 1;
    repeat
        Inc(Ops[False].Full);
        Video^.PutText(X, Y, TestStr, C);
        Video^.Update;
        Inc(Y, CH);
        if Y + CH >= GetMaxY then begin
            Y := 0;
            Inc(C);
            if C >= Video^.Colors then
                C := 0;
        end;
    until Elapsed(false);
    CW := Video^.TextWidth(TestStr);
    X := -CW div 2;
    Y := -CH div 2;
    D := 0;
    if not ResetTimer then exit;
    repeat
        Inc(Ops[False].Half);
        Video^.PutText(X, Y, TestStr, C);
        Video^.Update;
        MoveXY2(CW, CH, 2, X, Y, D);
        if D = 0 then begin
            Inc(C);
            if C >= Video^.Colors then
                C := 0;
        end;
    until Elapsed(false);
    X := (CW div 4) - CW;
    Y := (CH div 4) - CH;
    D := 0;
    if not ResetTimer then exit;
    repeat
        Inc(Ops[False].Qtr);
        Video^.PutText(X, Y, TestStr, C);
        Video^.Update;
        MoveXY2(CW, CH, 4, X, Y, D);
        if D = 0 then begin
            Inc(C);
            if C >= Video^.Colors then
                C := 0;
        end;
    until Elapsed(false);
end;

procedure BenchTestMode(var Ops : TBench);
begin
    if not ResetTimer then Exit;
    repeat
    until Elapsed(False);
    { Raw screen updates }
    if not ResetTimer then Exit;
    repeat
        Video^.UpdateForce;
        Video^.Update;
        IncCount(Ops.FullUpdate);
    until Elapsed(False);
    {$IFNDEF DEV}
    ScreenFills(Ops);
    ScreenShifting(Ops);
    PixelDrawing(Ops);
    ImageDrawing(Ops);
    MaskDrawing(Ops);
    CharTest(Ops.Chars);
    StrTest(Ops.Strs);
    {$ELSE}
    ScreenShifting(Ops);
    {$ENDIF}
end;

procedure BlankScreenMessage(Name : String);
var
    S : String;
begin
    Video^.Fill(0);
    S := Name + ' video test';
    Video^.PutText(GetMaxX div 2 - Video^.TextWidth(S) div 2 ,
        GetMaxY div 2 - Video^.TextHeight(S) * 2, S, 7);

    S := 'this message will be displayed for ' + intstr(SecsPer * 2) + ' seconds';
    Video^.PutText(GetMaxX div 2 - Video^.TextWidth(S) div 2 ,
        GetMaxY div 2 - Video^.TextHeight(S) div 2, S, 7);
    S := 'while raw video screen updates are measured';
    Video^.PutText(GetMaxX div 2 - Video^.TextWidth(S) div 2 ,
        GetMaxY div 2 + Video^.TextHeight(S) div 2, S, 7);
    Video^.Update;
end;

procedure BenchTestDriver;
var
    I, E : integer;
    VideoModes : PVideoModes;
    P : PAsset;
begin
    VideoModes := Video^.GetModes;
    I := 0;
    while (VideoModes^[I].Mode <> 0) and (not AbortFlag) do begin
        E := Video^.Open(VideoModes^[I].Mode);
        if E <> 0 then
            FatalError(E, 'opening graphics mode 0x' +
                WordHex(VideoModes^[I].Mode));
        { HexDump(Video^.ExtendedData^, 16);
        Pause(0); }
        ModeInfo.Wide := Video^.Width;
        ModeInfo.High := Video^.Height;
        ModeInfo.Colors := Video^.Colors;
        if AssetLoad('1012N-EN.FNT', asDefault, P) then
            Video^.SetFont(PFont(P^.Data));
        Font := PFont(P^.Data);
        FillChar(ASync, Sizeof(ASync), 0);
        FillChar(Sync, Sizeof(Sync), 0);
        FillChar(UBuf, Sizeof(UBuf), 0);
        {$IFNDEF DEV}
        if TestSync then begin
            BlankScreenMessage('Synchronized');
            Video^.SetSync(True);
            BenchTestMode(Sync);
        end;
        if TestASync then begin
            BlankScreenMessage('Asynchronous');
            Video^.SetSync(False);
            BenchTestMode(ASync);
        end;
        {$ENDIF}
        if TestUBuf then begin
            BlankScreenMessage('Unbuffered');
            Video^.SetBuffered(False);
            BenchTestMode(UBuf);
        end;
        Video^.Close;
        BenchReport(VideoModes^[I].Mode);
        Inc(I);
        {$IFDEF DEV}
        break;
        {$ENDIF}
    end;
end;

procedure TestDriver(FileName : String);
var
    P : PAsset;
begin
    if not AssetLoad(FileName, asDefault, P) then begin
        if GetError <> 0 then
            FatalError(GetError, 'driver ' + FileName + ' error');
        Exit;
    end;
    if PDriver(P^.Data)^.Class <> dcVideo then begin
        FatalError(erOperation_Not_Supported, FileName + ' is not a video driver');
        exit; { not needed at present, but eventually will not kill test on bad driver }
    end;
    {$IFNDEF DEV}
    WriteLn;
    Write('Testing the ', PDriver(P^.Data)^.Name, ' driver will take nearly 20 minutes ');
    WriteLn('for each video mode.');
    WriteLn;
    WriteLn('You can abort the test early by pressing a key.');
    WriteLn;
    WriteLn('The benchmark report is very long. So, you should redirect output to a file.');
    WriteLn('Otherwise, you will not be able to read the report and are probably just');
    WriteLn('wasting your time.');
    Pause(10000);
    WriteLn;
    {$ENDIF}
    Video := PVideoDriver(P^.Data);
    BenchTestDriver;
    Video := nil;
    AssetUnlock(P);
    WriteLn;
    WriteLn('Total test duration roughly: ', Duration, ' seconds' );
    WriteLn;
    WriteLn('Some image/mask functions were rendered using "XOR Put Mode" and would have');
    WriteLn('appeared to be garbled. Since no consideration was given to any pre-existing');
    WriteLn('pixels, that is normal and NOT A BUG. The purpose is to test the performance');
    WriteLn('of those functions. Not to look pretty.');
end;

var
    I : integer;
    DT : boolean;
begin
    AssetIndexSys('DRIVERS', '');
    AssetIndexSys('FONTS', '');
    DT := false;
    if ParamCount > 0 then begin
        for I := 1 to ParamCount do begin
            if UCase(ParamStr(I)) = 'SYNC' then begin
                TestSync := True;
                TestASync := False;
                TestUBuf := False;
            end else
            if UCase(ParamStr(I)) = 'ASYNC' then begin
                TestSync := False;
                TestASync := True;
                TestUBuf := False;
            end else
            if UCase(ParamStr(I)) = 'UBUF' then begin
                TestSync := False;
                TestASync := False;
                TestUBuf := True;
            end else
            if UCase(ParamStr(I)) = 'ALL' then begin
                TestSync := True;
                TestASync := True;
                TestUBuf := True;
            end else begin
                TestDriver(ParamStr(I));
                DT := True;
            end;
        end;
    end;
    if not DT then
         TestDriver('VGA386.DRV');
end.