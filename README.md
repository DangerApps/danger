# Danger Engine

DOS Application & Game Executable Resource Engine for 2D game development.

Requires VGA, 386+, Turbo Pascal, Nasm and QRESFILE from PGME.

### A work in progress...

The Danger Engine is a very early stage work in progress. At present, you "can"
make a game using it. But, many areas are still very rough around the edges. On
top of that, there is no sound or cpu speed management at all. Also, only one
very barebones VGA driver with much of it's needed functionality out-sourced to
the graphics sub-system. So graphics speeds are no where near the levels they
should reach once drivers are finished. On top of all of that, almost nothing
is optimized for performance, memory consumption or overall size.