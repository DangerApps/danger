{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

{$IFDEF SECTINT}


{$ENDIF}

{$IFDEF SECTIMP}
procedure DisposeSprite(var Sprite : PSprite);
var
    I : word;
begin
    if Assigned(Sprite^.Sprites) then begin
        for I := 0 to Sprite^.Count - 1 do begin
            Video^.FreeImage(Sprite^.Sprites^[I].Image);
            Video^.FreeImage(Sprite^.Sprites^[I].IMask);
            Video^.FreeMask(Sprite^.Sprites^[I].BMask);
        end;
        FreeMem(Sprite^.Sprites, Sprite^.Count * Sizeof(TMaskedImage));
    end;
    Video^.FreeImage(Sprite^.Behind);
    Dispose(Sprite);
    {$IFDEF LOGS} Log('Sprite Disposed'); {$ENDIF}
    Sprite := nil;
end;

procedure BuiltIn_SpriteSwap(var Sprites : TSpriteList; Undraw : boolean); far;
var
    Hold : TSpriteList;
begin
    if Undraw then Video^.SpriteUndrawAll;
    Hold := SpriteList;
    SpriteList := Sprites;
    Sprites := Hold;
    SpriteList.Flag := True;
end;

function BuiltIn_NewSprite (Width, Height, Count : word; Populate : boolean) : PSprite; far;
var
    Sprite : PSprite;
    I  : word;
begin
    if Count = 0 then
        Sprite := nil
    else
        Sprite := New(PSprite);
    {$IFDEF LOGS} Log('New Sprite: ' + IntStr(Width) + 'x' + IntStr(Height) + 'x' +
        IntStr(Count) ); {$ENDIF}
    if Assigned(Sprite) then begin
        FillChar(Sprite^, Sizeof(TSprite), 0);
        Sprite^.Behind := Video^.NewImage(Width, Height);
        if Assigned(Sprite^.Behind) then
            GetMem(Sprite^.Sprites, Count * Sizeof(TMaskedImage));
        if Assigned(Sprite^.Sprites) then begin
            FillChar(Sprite^.Sprites^, Count * Sizeof(TMaskedImage), 0); { nill pointers }
            Sprite^.Count := Count;
            if Populate then begin
                for I := 0 to Count - 1 do begin
                    Sprite^.Sprites^[I].Image := Video^.NewImage(Width, Height);
                    Sprite^.Sprites^[I].IMask := nil;
                    Sprite^.Sprites^[I].BMask := nil;
                    Sprite^.Sprites^[I].Sequence := 0;
                    if not Assigned(Sprite^.Sprites^[I].Image) then Break;
                    Sprite^.Sprites^[I].BMask := Video^.NewMask(Width, Height);
                    if not Assigned(Sprite^.Sprites^[I].BMask) then Break;
                    { if ImageCompress then begin
                        Video^.ImageImplode(Sprite^.Sprites^[I].Image);
                        Video^.ImageImplode(Sprite^.Sprites^[I].IMask);
                    end; }
                end;
            end;
        end;
    end;
    if NoError and Assigned(Sprite) then begin
        Sprite^.Kind := skDefault;
        Sprite^.Level := slDefault;
        SetArea(0,0, Width - 1, Height - 1, Sprite^.Area);
        Video^.SpriteAdd(Sprite);
    end else
        DisposeSprite(Sprite);
    BuiltIn_NewSprite := Sprite;
end;

procedure BuiltIn_FreeSprite(var Sprite : PSprite); far;
begin
    if not assigned(Sprite) then exit;
    Video^.SpriteRemove(Sprite);
    {$IFDEF LOGS} Log('Free Sprite '); {$ENDIF}
    DisposeSprite(Sprite);
    Sprite := nil;
end;

procedure BuiltIn_FreeAllSprites; far;
var
    P : PSprite;
begin
    Video^.SpriteUndrawAll;
    While Assigned(SpriteList.First) do begin
        P := SpriteList.First;
        Video^.FreeSprite(P);
    end;
end;

function BuiltIn_CloneSprite(Sprite : PSprite) : PSprite; far;
var
    I : integer;
    P : PSprite;
    Fail : boolean;
begin
    Fail := False;
    P := nil;
    if Assigned(Sprite) then
        P := Video^.NewSprite(
            Sprite^.Area.Right - Sprite^.Area.Left,
            Sprite^.Area.Bottom - Sprite^.Area.Top,
            Sprite^.Count,
            false);
    if Assigned(P) then begin
        P^.Kind         := Sprite^.Kind;
        P^.Level        := Sprite^.Level;
        P^.Animate      := Sprite^.Animate;
        P^.Area         := Sprite^.Area;
        P^.HotSpot      := Sprite^.HotSpot;
        P^.Index        := Sprite^.Index;
        for I := 0 to P^.Count - 1 do begin
            P^.Sprites^[I].Sequence := Sprite^.Sprites^[I].Sequence;
            if Assigned(Sprite^.Sprites^[I].Image) then begin
                P^.Sprites^[I].Image := Video^.CloneImage(Sprite^.Sprites^[I].Image);
                Fail := Fail or (not Assigned(P^.Sprites^[I].Image));
            end;
            if Assigned(Sprite^.Sprites^[I].IMask) then begin
                P^.Sprites^[I].IMask := Video^.CloneImage(Sprite^.Sprites^[I].IMask);
                Fail := Fail or (not Assigned(P^.Sprites^[I].IMask));
            end;
            if Assigned(Sprite^.Sprites^[I].BMask) then begin
                P^.Sprites^[I].BMask := Video^.CloneMask(Sprite^.Sprites^[I].BMask);
                Fail := Fail or (not Assigned(P^.Sprites^[I].BMask));
            end;
            if Fail then begin
                Video^.FreeSprite(P);
                Break;
            end;
        end;
    end;

    {$IFDEF LOGS}
        Log('Cloned Sprite ' + PtrHex(Sprite) + ' to ' + PtrHex(P) + ', ' +
            IntStr(SpriteSizeOf(Sprite)) + ' bytes');
    {$ENDIF}
    BuiltIn_CloneSPrite := P;
end;

function BuiltIn_SpriteSizeOf(Sprite : PSprite) : LongInt; far;
var
    Size : LongInt;
    I : Word;
begin
    Size := 0;
    if Assigned(Sprite) then begin
        Size := Sizeof(TSprite) + Sprite^.Count * Sizeof(TMaskedImage);
        for I := 0 to Sprite^.Count - 1 do begin
            Inc(Size, Video^.ImageSizeOf(Sprite^.Sprites^[I].Image));
            Inc(Size, Video^.MaskSizeOf(Sprite^.Sprites^[I].BMask));
            Inc(Size, Video^.ImageSizeOf(Sprite^.Sprites^[I].IMask));
        end;
    end;
    BuiltIn_SpriteSizeOf := Size;
end;

procedure BuiltIn_SpriteRemove(var Sprite : PSprite); far;
begin
    if not (Assigned(Sprite^.Prev) or Assigned(Sprite^.Next) or (Sprite = SpriteList.First)) then exit;
    Video^.SpriteUndraw(Sprite);
    if Assigned(Sprite^.Prev) then
        Sprite^.Prev^.Next := Sprite^.Next
    else
        SpriteList.First := Sprite^.Next;
    if Assigned(Sprite^.Next) then
        Sprite^.Next^.Prev := Sprite^.Prev
    else
        SpriteList.Last := Sprite^.Prev;
    Sprite^.Prev := nil;
    Sprite^.Next := nil;
end;

procedure SpriteTopMost(var Sprite : PSprite); far;
begin
    Video^.SpriteRemove(Sprite);
    if Assigned(SpriteList.First) then begin
        Sprite^.Next := SpriteList.First;
        SpriteList.First^.Prev := Sprite;
    end else begin
        SpriteList.Last := Sprite;
    end;
    SpriteList.First := Sprite;
end;

procedure SpriteBottomMost(var Sprite : PSprite); far;
begin
    Video^.SpriteRemove(Sprite);
    if Assigned(SpriteList.Last) then begin
        Sprite^.Prev := SpriteList.Last;
        SpriteList.Last^.Next := Sprite;
    end else begin
        SpriteList.First := Sprite;
    end;
    SpriteList.Last := Sprite;
end;

procedure BuiltIn_SpriteSort; far;
var
    L, P : PSprite;
    R : boolean;
begin
	repeat
		R := True;
		P := SpriteList.Last;
		if not Assigned(P) then exit;
		repeat
			L := P^.Prev;
			if Not Assigned(L) then break;
			if L^.Level < P^.Level then begin
				SpriteTopMost(P);
				R := False;
			end;
			P := L;
		until false
	until R;
end;

procedure BuiltIn_SpriteAdd(var Sprite : PSprite); far;
begin
    SpriteTopMost(Sprite);
end;

procedure BuiltIn_SpriteSetVisible(var Sprite : PSprite; Visible : boolean); far;
begin
    if Visible then
        Video^.SpriteShow(Sprite)
    else
        Video^.SpriteHide(Sprite);
end;

procedure BuiltIn_SpriteUndraw (var Sprite : PSprite); far;
var
    UA : TArea;
    P : PSprite;
begin
    P := Sprite;
    if P^.Visible and (not P^.NeedsDrawn) then P^.NeedsUndrawn := True;
    UA := P^.Area;
    P := P^.Prev;
    while Assigned(P) do begin
        if (P^.Visible) and (not P^.NeedsDrawn) then begin
            if AreaOverLap(UA, P^.Area) then begin
                P^.NeedsUndrawn := True;
                if P^.Area.Left < UA.Left then UA.Left := P^.Area.Left;
                if P^.Area.Top < UA.Top then UA.Top := P^.Area.Top;
                if P^.Area.Right > UA.Right then UA.Right := P^.Area.Right;
                if P^.Area.Bottom > UA.Bottom then UA.Bottom := P^.Area.Bottom;
            end;
        end;
        P := P^.Prev;
    end;
    P := SpriteList.First;
    while Assigned(P) do begin
        if P^.NeedsUndrawn then begin
            P^.NeedsDrawn := True;
            P^.NeedsUndrawn := False;
            Video^.PutImage(P^.Behind, P^.Area.Left, P^.Area.Top);
            SpriteList.Flag := True;
        end;
        if P = Sprite then Break;
        P := P^.Next;
    end;
end;

procedure BuiltIn_SpriteUndrawArea (Area : TArea); far;
var
    P : PSprite;
begin
    P := SpriteList.Last;
    while Assigned(P) do begin
        if (P^.Visible) and (not P^.NeedsDrawn) then begin
            if AreaOverLap(Area, P^.Area) then begin
                P^.NeedsUndrawn := True;
                if P^.Area.Left < Area.Left then Area.Left := P^.Area.Left;
                if P^.Area.Top < Area.Top then Area.Top := P^.Area.Top;
                if P^.Area.Right > Area.Right then Area.Right := P^.Area.Right;
                if P^.Area.Bottom > Area.Bottom then Area.Bottom := P^.Area.Bottom;
            end;
        end;
        P := P^.Prev;
    end;
    P := SpriteList.First;
    while Assigned(P) do begin
        if P^.NeedsUndrawn then begin
            P^.NeedsDrawn := True;
            P^.NeedsUndrawn := False;
            Video^.PutImage(P^.Behind, P^.Area.Left, P^.Area.Top);
            SpriteList.Flag := True;
        end;
        P := P^.Next;
    end;
end;

procedure BuiltIn_SpriteUndrawAll; far;
var
    P : PSprite;
begin
    P := SpriteList.First;
    while Assigned(P) do begin
        if (P^.Visible) and (not P^.NeedsDrawn) then begin
            P^.NeedsDrawn := True;
            P^.NeedsUndrawn := False;
            Video^.PutImage(P^.Behind, P^.Area.Left, P^.Area.Top);
            SpriteList.Flag := True;
        end;
        P := P^.Next;
    end;
end;

procedure BuiltIn_SpriteShow (var Sprite : PSprite); far;
begin
    if Sprite^.Visible then exit;
    if not Assigned(Sprite^.Sprites^[Sprite^.Index].Image) then exit;
    Video^.SpriteUndraw(Sprite);
    Sprite^.Visible := True;
    Sprite^.NeedsDrawn := True;
    SpriteList.Flag := True;
end;

procedure BuiltIn_SpriteHide (var Sprite : PSprite); far;
begin
    if Not Sprite^.Visible then exit;
    Video^.SpriteUndraw(Sprite);
    Sprite^.Visible := False;
    Sprite^.NeedsDrawn := False;
    SpriteList.Flag := True;
end;

procedure BuiltIn_SpriteChange (var Sprite : PSprite; Index : word); far;
begin
    if Index > Sprite^.Count - 1 then Index := 0;
    if Sprite^.Index = Index then exit;
    if Sprite^.Visible then
        Video^.SpriteUndraw(Sprite);
    Sprite^.Index := Index;
end;

procedure BuiltIn_SpriteNext (var Sprite : PSprite); far;
var
   TI, I, Seq : word;
begin
    if (Sprite^.Count < 2) or (Sprite^.Animate = 0) then exit;
    if not Assigned(Sprite^.Sprites^[Sprite^.Index].Image) then exit;
    Seq := Sprite^.Sprites^[Sprite^.Index].Sequence;
    I := Sprite^.Index;
    TI := I;
    repeat
        Inc(I);
        if I > Sprite^.Count - 1 then
            begin
                I := 0;
                if (Sprite^.Animate > 0) then begin
                    Dec(Sprite^.Animate);
                    if (Sprite^.Animate = 0) then begin
                        I := TI;
                        Break;
                    end;
                end;
            end;
        if Assigned(Sprite^.Sprites^[I].Image) and (Seq = Sprite^.Sprites^[I].Sequence) then Break;
    until I = Sprite^.Index;
    Video^.SpriteChange(Sprite, I);
end;

procedure BuiltIn_SpriteNextAll(Hidden : boolean); far;
var
    X : PSprite;
begin
    X := SpriteList.Last;
    while Assigned(X) do begin
        if X^.Visible or Hidden then
            Video^.SpriteNext(X);
        X := X^.Prev;
    end;
end;

procedure AdjustedSpriteArea(var Sprite : PSprite;  x, y : integer; var Area : TArea);
var
    CX, CY : integer;
begin
    Area := Sprite^.Area;
    Dec(X, Sprite^.HotSpot.X);
    Dec(Y, Sprite^.HotSpot.Y);
    if (X = Area.Left) and (Y = Area.Top) then exit;
    CX := X - Area.Left;
    CY := Y - Area.Top;
    Inc(Area.Left, CX);
    Inc(Area.Right, CX);
    Inc(Area.Top, CY);
    Inc(Area.Bottom, CY);
end;

procedure BuiltIn_SpriteMove (var Sprite : PSprite; x, y : integer); far;
var
    CX, CY : integer;
begin
    X := X - Sprite^.HotSpot.X;
    Y := Y - Sprite^.HotSpot.Y;
    if (X = Sprite^.Area.Left) and (Y = Sprite^.Area.Top) then exit;
    if Sprite^.Visible then
        Video^.SpriteUndraw(Sprite);
    CX := X - Sprite^.Area.Left;
    CY := Y - Sprite^.Area.Top;
    Inc(Sprite^.Area.Left, CX);
    Inc(Sprite^.Area.Right, CX);
    Inc(Sprite^.Area.Top, CY);
    Inc(Sprite^.Area.Bottom, CY);
    if Sprite^.Visible then
        Video^.SpriteUndraw(Sprite);
end;

procedure BuiltIn_SpriteWhere (var Sprite : PSprite; var Where : TPoint); far;
begin
    Where.X := Sprite^.Area.Left + Sprite^.HotSpot.X;
    Where.Y := Sprite^.Area.Top + Sprite^.HotSpot.Y;
end;

function BuiltIn_SpriteOver (var Sprite : PSprite; Pixel : TPoint) : boolean; far;
begin
    BuiltIn_SpriteOver := False;
    if not Assigned(Sprite) then exit;
    if not Sprite^.Visible then exit;
    BuiltIn_SpriteOver := PixelInArea(Pixel.x,Pixel.y,Sprite^.Area);
end;

function BuiltIn_SpriteOverArea(var Sprite : PSprite; Area : TArea) : boolean; far;
begin
    BuiltIn_SpriteOverArea := False;
    if not Assigned(Sprite) then exit;
    if not Sprite^.Visible then exit;
    BuiltIn_SpriteOverArea := AreaOverLap(Area, Sprite^.Area);
end;

function BuiltIn_SpriteWillCover (var Sprite : PSprite; x, y : integer; Pixel : TPoint) : boolean; far;
var
    Area : TArea;
begin
    BuiltIn_SpriteWillCover := False;
    if not Assigned(Sprite) then exit;
    if not Sprite^.Visible then exit;
    AdjustedSpriteArea(Sprite, x, y, Area);
    if not PixelInArea(Pixel.X, Pixel.Y, Area) then exit;
    with Sprite^.Sprites^[Sprite^.Index] do
        if Assigned(BMask) then begin
            { Check Mask Bits }
            if BMask^.Compression <> icaUncompressed then
                Video^.MaskExplode(BMask);
            X := Pixel.X - X;
            Y := Pixel.Y - Y;
            Y := (Y * BMask^.ByteWidth) + (X shr 3);
            X := 7 - (X and 7);
            if BMask^.ImageData[Y] and (1 shl X) <> 0 then
                Exit;
        end;
    BuiltIn_SpriteWillCover := True;
end;

function MaskIntersect(Mask, WithMask : PMask; X, Y : integer) : boolean;
var
    XX, YY,
    SX,
    SR, DR : integer;
begin
    MaskIntersect := True;
    for YY := 0 to WithMask^.Height - 1 do begin
        if (YY + Y < 0) or (YY + Y >= Mask^.Height) then continue;
        SR := (YY + Y * Mask^.ByteWidth);
        DR := ((YY) * WithMask^.ByteWidth);
        for XX := 0 to WithMask^.Width - 1 do begin
            SX := XX + X;
            if (SX < 0) or (SX >= Mask^.Width) then continue;
            if (Mask^.ImageData[SR + SX shr 3] and (1 shl (7 - (SX and 7))) = 0)
            and (WithMask^.ImageData[DR + XX shr 3] and (1 shl (7 - (XX and 7))) = 0) then
                Exit;
        end;
    end;
    MaskInterSect := False;
end;

function AdjustedIntersect(Sprite, WithSprite : PSprite; Area : TArea) : boolean;
begin
    AdjustedIntersect := False;
    if Sprite = WithSprite then exit;
    if not (Assigned(WithSprite) and WithSprite^.Visible) then exit;
    if Sprite^.Level <> WithSprite^.Level then exit;
    if not AreaOverLap(Area, WithSprite^.Area) then exit;
    if (not Assigned(Sprite^.Sprites^[Sprite^.Index].BMask))
    or (not Assigned(WithSprite^.Sprites^[WithSprite^.Index].BMask)) then
        AdjustedIntersect := True
    else
        AdjustedIntersect := MaskIntersect(
            Sprite^.Sprites^[Sprite^.Index].BMask,
            WithSprite^.Sprites^[WithSprite^.Index].BMask,
            WithSprite^.Area.Left - Area.Left, WithSprite^.Area.Top - Area.Top);
end;

function BuiltIn_SpriteIntersect (var Sprite : PSprite; x, y : integer; WithSprite : PSprite) : boolean; far;
var
    Area : TArea;
begin
    BuiltIn_SpriteIntersect := False;
    if not (Assigned(Sprite) and Sprite^.Visible) then exit;
    AdjustedSpriteArea(Sprite, x, y, Area);
    BuiltIn_SpriteIntersect := AdjustedIntersect(Sprite, WithSprite, Area);
end;

function BuiltIn_SpriteCollide (var Sprite : PSprite; x, y : integer) : PSprite; far;
var
    Area : TArea;
    P : PSprite;
begin
    BuiltIn_SpriteCollide := nil;
    if not Assigned(Sprite) then exit;
    if not (Assigned(Sprite) and Sprite^.Visible) then exit;
    AdjustedSpriteArea(Sprite, x, y, Area);
    P := SpriteList.First;
    while Assigned(P) do begin
        if AdjustedIntersect(Sprite, P, Area) then break;
        P := P^.Next;
    end;
    BuiltIn_SpriteCollide := P;
end;

function BuiltIn_SpriteGetSeq(var Sprite : PSprite) : word; far;
begin
    if Assigned(Sprite) and Assigned(Sprite^.Sprites) then
        BuiltIn_SpriteGetSeq := Sprite^.Sprites^[Sprite^.Index].Sequence
    else
        BuiltIn_SpriteGetSeq := 0;
end;

function BuiltIn_SpriteSetSeq(var Sprite : PSprite; Seq : word) : word; far;
var
    I, Prev : word;
begin
    if Assigned(Sprite) and Assigned(Sprite^.Sprites) then begin
        if  Sprite^.Sprites^[Sprite^.Index].Sequence <> Seq then begin
            for I := 0 to Sprite^.Count - 1 do
                if Sprite^.Sprites^[I].Sequence = Seq then begin
                    Video^.SpriteCHange(Sprite, I);
                    Break;
                end;
        end;
        BuiltIn_SpriteSetSeq := Sprite^.Sprites^[Sprite^.Index].Sequence
    end else
        BuiltIn_SpriteSetSeq := 0;
end;

procedure BuiltIn_UpdateSprites; far;
var
    P : PSprite;
begin
    if not SpriteList.Flag then Exit;
    SpriteList.Flag := False;
    P := SpriteList.Last;
    while Assigned(P) do begin
        if (P^.Visible) and P^.NeedsDrawn then begin
            P^.NeedsDrawn := False;
            With P^ do begin
                Video^.GetImage(Behind, Area.Left, Area.Top);
                if Assigned(Sprites^[Index].BMask) then begin
                    Video^.PutMaskMode(Sprites^[Index].BMask,
                        Area.Left, Area.Top, imAND);
                    Video^.PutImageMode(Sprites^[Index].Image,
                        Area.Left, Area.Top, imXOR);
                end else
                if Assigned(Sprites^[Index].iMask) then begin
                    Video^.PutImageMode(Sprites^[Index].IMask,
                        Area.Left, Area.Top, imAND);
                    Video^.PutImageMode(Sprites^[Index].Image,
                        Area.Left, Area.Top, imXOR);
                end else
                    Video^.PutImage(Sprites^[Index].Image,
                        Area.Left, Area.Top);
            end;
        end;
        P := P^.Prev;
    end;
end;

{$ENDIF}