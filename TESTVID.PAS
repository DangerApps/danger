{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

{$I DANGER.DEF}
program Video_Test_Application;

uses Danger;

procedure PrintPalletes(P : TRGBPalettes);
var
    I : integer;
begin
    for I := 0 to Video^.Colors - 1 do
        WriteLn('c:', ByteHex(I),
            ', r:', ByteHex(P[I].Red),
            ', g:', ByteHex(P[I].Green),
            ', b:', ByteHex(P[I].Blue));
end;

function Circling(var X, Y, D : integer; W, H : integer; Steps : integer) : boolean;
const
    SX : integer = 0;
    SY : integer = 0;
begin
    Circling := True;
    case D of
        0 : begin
            Inc(D);
            SX := X;
            SY := Y;
        end;
        1 : begin
            if Y + H < Video^.Height - SY then
                Inc(Y, Steps)
            else
                Inc(D);
        end;
        2 : begin
            if X + W  < Video^.Width - SX then
                Inc(X, Steps)
            else
                Inc(D);
        end;
        3 : begin
            if Y > SY then
                Dec(Y, Steps)
            else
                Inc(D);
        end;
        4 : begin
            if X - Steps * 2 > SX then
                Dec(X, Steps)
            else
                Inc(D);
        end;
        5 : begin
            D := 0;
            Circling := False;
        end;
    end;
end;

procedure FontTests;
var
    I, X, Y : integer;
    S : String;
begin
    S:= 'Say hello to my little friend.';
    Y := Video^.TextHeight(S);
    X := Video^.Width div 2 - Video^.TextWidth(S) div 2;
    for I := 0 to 8 do begin
        Video^.PutText(X,Y,S,15);
        Video^.Update;
        if not Pause(250) then break;
        Video^.PutText(X,Y,S,0);
        Video^.Update;
        if not Pause(250) then break;
    end;
    S := 'The quick fox jumped over the lazy dog.';
    Inc(Y,Video^.TextHeight(S));
    X := Video^.Width div 2 - Video^.TextWidth(S) div 2;
    I := 0;
    while Y < Video^.Height do begin
        Inc(I);
        if I >= Video^.Colors then I := 0;
        Inc(Y,Video^.TextHeight(S));
        Video^.PutText(X,Y,S,I);
    end;
end;

procedure ColorTest;
var
    I, J: integer;
begin
    J := 0;
    for I := 0 to Video^.Width - 1 do begin
        Video^.Region(I, 0, I, Video^.Height -1, J);
        Inc(J);
        if J >= Video^.Colors then J := 0;
    end;
    Video^.Update;
end;

procedure ColorBoxes;
var
    I, J: integer;
begin
    J := 0;
    for I := 0 to Video^.Height div 2 do begin
        Video^.Region(I, I, GetMaxX - I - 1, GetMaxY - I - 1, J);
        Inc(J);
        if J >= Video^.Colors then J := 0;
    end;
    Video^.Update;
end;

procedure FontTest;
var
    P : PAsset;
    S : String;
    Fonts : String;
begin
    Fonts:='1012n-en.fnt;1214n-en.fnt;0816n-en.fnt;0816n-tr.fnt';
    while PullStr(';', Fonts, S) do begin
        if not AssetLoad(S, asDefault, P) then
            if GetError <> 0 then
                FatalError(GetError, 'loading ' + S + 'font');
        if not Assigned(P) then Continue;
        ColorTest;
        Video^.SetFont(PFont(P^.Data));
        FontTests;
        Video^.Update;
        Video^.SetFont(nil);
        AssetUnlock(P);
        if Assigned(MousePtr) then begin
            Pause(10000);
            Break;
        end else
            Pause(1000);
    end;
end;

procedure ShiftTestC;
var
    I, J : word;
begin
    ColorTest;
    Video^.update;
    for J := 0 to 10 do begin
        for I := 1 to 9 do begin
            Video^.Shift(I, 1 + J, I * 2 + J);
            Video^.Update;
        end;
    end;
    Pause(1500);
end;

procedure ShiftTestDirLoop(dir : word; c : tcolor);
var
    LTT : longint;
    I, J : word;
begin
    for I := 1 to 25 do begin
        Video^.ShiftRegion(20, 20, GetMaxX - 20, GetMaxY - 20, dir, 2, C);
        Video^.Update;
        {for J := 0 to 6 do
            Write(Words(Video^.ExtendedData^)[J], ' ');
        WriteLn; }
        { While LTT = TimerTick do;
        LTT := TimerTick; }
    end;
end;

procedure ShiftTestA;
var
    I : integer;
begin
    Video^.Region(9, 9, GetMaxX - 9, GetMaxY - 9, 1);
    Video^.Region(10, 10, GetMaxX - 10, GetMaxY - 10, 15);
    Video^.Region(11, 11, GetMaxX - 11, GetMaxY - 11, 8);
    Video^.Region(60, 60, GetMaxX - 10, GetMaxY - 10, 15);
    Video^.Region(61, 61, GetMaxX - 11, GetMaxY - 11, 8);
    Video^.update;
    for I := 1 to 5 do begin
        ShiftTestDirLoop(dmUp, 0);
        ShiftTestDirLoop(dmLeft, 0);
        ShiftTestDirLoop(dmDown, 0);
        ShiftTestDirLoop(dmRight, 0);
    end;
    Pause(1000);
end;

procedure ShiftTestB;
var
    I : integer;
begin
    Video^.Region(19, 19, GetMaxX - 19, GetMaxY - 19, 1);
    Video^.Region(20, 20, GetMaxX - 20, GetMaxY - 20, 7);
    Video^.Region(21, 21, GetMaxX - 21, GetMaxY - 21, 8);
    Video^.Region(70, 120, GetMaxX - 70, GetMaxY - 20, 15);
    Video^.Region(71, 121, GetMaxX - 71, GetMaxY - 21, 4);
    Video^.update;
    for I := 4 downto 0 do begin
        ShiftTestDirLoop(dmUpLeft, I * 3);
        ShiftTestDirLoop(dmUpRight, I * 3);
        ShiftTestDirLoop(dmDownRight, I * 3);
        ShiftTestDirLoop(dmDownLeft, I * 3);
    end;
    Pause(1000);
end;

procedure ShiftTestD;
var
    B : PImage;
    I : integer;
    J : LongInt;
    E : TEvent;
    S : String;
    F : PFont;
begin
    S := '1012N-EN.FNT';
    FontBestMatch(S,F);
    Video^.SetFont(F);

    B := nil;
    Video^.Region(19, 19, GetMaxX - 19, GetMaxY - 19, 1);
    Video^.Region(20, 20, GetMaxX - 20, GetMaxY - 20, 7);
    Video^.Region(21, 21, GetMaxX - 21, GetMaxY - 21, 8);
    Video^.Region(70, 120, GetMaxX - 70, GetMaxY - 20, 15);
    Video^.Region(71, 121, GetMaxX - 71, GetMaxY - 21, 4);
    Video^.update;
    J := 0;
    B := Video^.NewImage(160, 14);
    Video^.GetImage(B, 80,2);
    repeat
        Inc(J);
        S := IntStr(J);
        Video^.PutImage(B, 80, 2);
        Video^.PutText(GetMaxX div 2 - Video^.TextWidth(S) div 2, 2, S, 15);
        Video^.Update;
        for I := 4 downto 0 do begin
            ShiftTestDirLoop(dmUpLeft, I * 3);
            ShiftTestDirLoop(dmUpRight, I * 3);
            ShiftTestDirLoop(dmDownRight, I * 3);
            ShiftTestDirLoop(dmDownLeft, I * 3);
        end;
        GetEvent(E);
    until E.Kind = evKeyPress;
    Video^.SetFont(nil);
end;

procedure ShiftTestE(Direct : TDirection);
var
    B : PImage;
    I : integer;
    J : LongInt;
    E : TEvent;
    S : String;
    F : PFont;
    C : TColor;
    A : TArea;
begin
    Video^.SetSync(False);
    Video^.SetBuffered(False);
    S := '1012N-EN.FNT';
    FontBestMatch(S,F);
    Video^.SetFont(F);
    J := 0;
    B := Video^.NewImage(160, 14);
    Video^.GetImage(B, 80,2);
    SetArea(20, 20, 300, 180, A);
    repeat
        if J mod 100 = 0 then begin
            S := IntStr(J);
            Video^.PutImage(B, 80, 2);
            Video^.PutText(GetMaxX div 2 - Video^.TextWidth(S) div 2, 2, S, 15);
        end;
        if (J shr 2) and 1 = 1 then C := 6 else C := 8;
        Video^.ShiftArea(A, Direct, 1, C);
        Video^.Update;
        GetEvent(E);
        Inc(J);
    until E.Kind = evKeyPress;
    Video^.SetFont(nil);
end;

procedure MaskTest;
var
    Image, Back : PImage;
    Mask : PMask;
    I, X, Y, D : integer;
begin
    { ColorTest; }
    Image := Video^.NewImage(24,24);
    Back := Video^.CloneImage(Image);
    Video^.ImageFill(Image, 0);
    for I := 0 to Image^.Height div 4 do
        Video^.ImageRegion(Image, 0, I * 4, Image^.Width - 1, I * 4 + 1, 15 - I );
    for I := 0 to Image^.Width div 4 do
        Video^.ImageRegion(Image, I * 4, 0, I * 4 + 1, Image^.Height - 1, 1 + I );
    Mask := Video^.ImageToMask(Image, 0);
    Video^.MaskImplode(Mask);
{    for I := 0 to 4 do
        Video^.PutMaskMode(Mask, 40 * I, 100, I); }

    X := Image^.Width - 1;
    Y := Image^.Height - 1;
    D := 0;
    for I := 0 to 2 do begin
        while  Circling(X, Y, D, Image^.Width, Image^.Height, 4) do begin
            Video^.GetImage(Back, X,Y);
            Video^.PutMask(Mask, X,Y);
            Video^.PutImageMode(Image, X,Y, imXOR);
            Video^.Update;
            Video^.PutImage(Back, X,Y);
            if D = 2 then Break;
        end;
        if D = 2 then Break;
        Inc(Y, 8);
    end;
    Video^.Update;
    Pause(1000);
    Video^.FreeImage(Image);
    Video^.FreeImage(Back);
    Video^.FreeMask(Mask);
end;

procedure MaskTestOdd;
var
    Image, Back : PImage;
    Mask : PMask;
    I, X, Y, D, Sz : integer;
begin
    { ColorTest; }
    for sz := 24 to 24 do begin
    Image := Video^.NewImage(sz,sz);
    Back := Video^.CloneImage(Image);
    Video^.ImageFill(Image, 0);
    for I := 0 to (Image^.Height - 1) div 4 do
        if I * 4 + 1 < Image^.Height then
        Video^.ImageRegion(Image, 0, I * 4, Image^.Width - 1, I * 4 + 1, 15 - I );
    for I := 0 to (Image^.Width - 1) div 4 do
        if I * 4 + 1 < Image^.Width then
        Video^.ImageRegion(Image, I * 4, 0, I * 4 + 1, Image^.Height - 1, 1 + I );
    Mask := Video^.ImageToMask(Image, 0);
    Video^.MaskImplode(Mask);
    { Video^.MaskExplode(Mask); }
    { for I := 0 to 4 do
        Video^.PutMaskMode(Mask, 40 * I, 100, I); }

    X := Image^.Width - 1;
    Y := Image^.Height - 1;
    D := 0;
    for I := 0 to 2 do begin
        while  Circling(X, Y, D, Image^.Width, Image^.Height, 4) do begin
            Video^.GetImage(Back, X,Y);
            Video^.PutMaskMode(Mask, X,Y, imCopy);
            {Video^.PutImageMode(Image, X,Y, imXOR);}
            Video^.Update;
            Delay(200);
            Video^.PutImage(Back, X,Y);
            if D = 2 then Break;
        end;
        if D = 2 then Break;
        Inc(Y, 8);
    end;
    Video^.Update;
    Pause(1000);
    Video^.FreeImage(Image);
    Video^.FreeImage(Back);
    Video^.FreeMask(Mask);
    end;
end;

procedure ImageTest;
var
    Image, Back : PImage;
    X, Y, D : integer;
begin
    Image := Video^.NewImage(48,48);
    Back := Video^.NewImage(48,48);
    { FillChar(Image^.Data, Video^.ImageSizePixels(Image), 1); }
    Video^.ImageFill(Image, Video^.Colors - 1);
    for X := 0 to 15 do
        Video^.ImageRegion(Image, X + 1, X + 1,
        Image^.Width - X - 2, Image^.Height - X - 2, 15 - X);
    Y := 16 - Image^.Height + 1;
    X := 16 - Image^.Width + 1;
    D := 0;
    while Circling(X, Y, D, Image^.Width, Image^.Height, 8) do begin
        Video^.GetImage(Back, X,Y);
        Video^.PutImage(Image, X,Y);
        Video^.Update;
        Video^.PutImage(Back, X,Y);
        { if D = 3 then Break; }
    end;
    Video^.Update;
    Pause(1000);
    Video^.FreeImage(Image);
    Video^.FreeImage(Back);
end;

procedure ImageTestB;
var
    Image, Back : PImage;
    X, Y, D : integer;
begin
    Image := Video^.NewImage(32,48);
    { FillChar(Image^.Data, Video^.ImageSizePixels(Image), 1); }
    Video^.ImageFill(Image, Video^.Colors - 1);
    for X := 0 to 15 do
        Video^.ImageRegion(Image, X + 1, X + 1,
        Image^.Width - X - 2, Image^.Height - X - 2, 15 - X);
    Y := 1;
    X := 1;
    Video^.PutImage(Image, X,Y);
    Video^.Update;
    Pause(1500);
    Video^.FreeImage(Image);
end;

procedure FadeTest;
var
    Pal : TRGBPalettes;
begin
    Video^.GetPalettes(Pal);
    Video^.FadeOut(Pal);
    Video^.FadeIn(Pal);
end;

procedure FillTest;
var
    I: integer;
begin
    for I := 0 to Video^.Colors - 1 do begin
        Video^.Prepare;
        Video^.Fill(I);
        Video^.Update;
    end;
end;

procedure MouseTest;
begin
     { Animated sprite/mouse test }
    MouseDefault(True);
    FontTest;
    Video^.FreeSprite(MousePtr);
end;

procedure ExtData;
var
    I : word;
    P : PWords;
begin
    P := Video^.ExtendedData;
    for I := 0 to 15 do
        WriteLn(ByteHex(I * 2), ':=', WordHex(P^[I]), ' / ', P^[I], ' / ', Integer(P^[I]));
end;

procedure DevTestShift;
var
    P : PImage;
    Area : TArea;
begin
    Video^.Fill(1);
    P := Video^.NewImage(50,50);
    Video^.ImageFill(P, 2);
    SetArea(10,20,30,40, Area);
    Video^.ImageFillArea(P, Area,4);
    SetArea(15,10,25,45, Area);
    Video^.ImageFillArea(P, Area,1);
    Video^.PutImage(P, 200, 100);
    Video^.FreeImage(P);
    SetArea(50, 100, 150, 200, Area);
{    Video^.ShiftArea(Area, dmUp, 1, 7); }
    Video^.Update;
    ShiftTestE(dmUp);
end;

procedure OverlapTest;
var
    S1, S2,ST : PSprite;
    P : PAsset;
    Abort : boolean;
    E : TEvent;
    R : LongInt;
    F : PFont;
    S : String;
    X, Y : integer;
    LTT : LongInt;
begin
    Video^.SetSync(False);
    Video^.SetBuffered(False);
    S := '1012N-EN.FNT';
    FontBestMatch(S,F);
    Video^.SetFont(F);
    S1 := AssetLoadOrDie('SPRITE1.IGS', asDefault, P);
    S2 := AssetLoadOrDie('SPRITE2.IGS', asDefault, P);
    Video^.Fill(0);
    X := GetMaxX div 2 - AreaWidth(S1^.Area);
    Y := GetMaxY div 2;
    Video^.SpriteMove(S1, X, Y);
    Video^.SpriteMove(S2, GetMaxX div 2 + 1, GetMaxY div 2);
    Video^.SpriteShow(S1);
    Video^.SpriteShow(S2);
    Abort := False;
    LTT := TimerTick;
    R := 0;
    ST := nil;
    repeat
        if GetEvent(E) and (E.Kind = evKeypress) then
            case E.ScanCode of
                scEscape : Abort := true;
                scUp : begin
                    Dec(Y);
                    Video^.SpriteMove(s1, X, Y);
                end;
                scDown : begin
                    Inc(Y);
                    Video^.SpriteMove(s1, X, Y);
                end;
                scLeft : begin
                    Dec(X);
                    Video^.SpriteMove(s1, X, Y);
                end;
                scRight : begin
                    Inc(X);
                    Video^.SpriteMove(s1, X, Y);
                end;

            end;
        if LTT <> TimerTick then begin
            LTT := TimerTick;
            Video^.Region(0,0, GetMaxX, 14, 1);
            ST := Video^.SpriteCollide(S1, X, Y);
            if Assigned(ST) then
                Video^.PutText(0,0, IntStr(R), 4)
            else
                Video^.PutText(0,0, IntStr(R), 2);
            R := 0;
        end else
            Inc(R);
        Video^.Update;
    until Abort;
    Video^.SetFont(nil);
end;

procedure GraphicsDemo;
begin
{   FillTest;
    FontTest;
    FadeTest;
    ColorBoxes;
    ShiftTestA;
    ShiftTestB;
    ShiftTestC;
    ColorBoxes;
    ImageTest;
    ColorBoxes;
    MaskTest;
    MaskTestOdd;
    MouseTest; }

    (* ShiftTestD;
    DevTestShift;*)
    OverlapTest;
end;

procedure VideoModeDemo;
var
    I, E : integer;
    VideoModes : PVideoModes;
    P : TRGBPalettes;
begin
    VideoModes := Video^.GetModes;
    I := 0;
    while VideoModes^[I].Mode <> 0 do begin
        E := Video^.Open(VideoModes^[I].Mode);
        if E <> 0 then
            FatalError(E, 'opening graphics mode 0x' +
                WordHex(VideoModes^[I].Mode));
        WriteLn(Video^.Name);
        WriteLn('Mode ' + WordHex(VideoModes^[I].Mode));
        WriteLn(IntStr(Video^.Width) + 'x' + IntStr(Video^.Height) + ' ' +
            IntStr(1 shl Video^.ColorDepth) + ' colors');
        { Pause(1500); }
        GraphicsDemo;
        Pause(500);
        Video^.Close;
        Inc(I);
        { if Video^.GetColorProfile(0, P) then
            PrintPalletes(P); }
    end;
end;

procedure TestDriver(FileName : String);
var
    P : PAsset;
begin
    if not AssetLoad(FileName, asDefault, P) then begin
        if GetError <> 0 then
            FatalError(GetError, 'driver ' + FileName + ' error');
        Exit;
    end;
    if PDriver(P^.Data)^.Class <> dcVideo then
        FatalError(erOperation_Not_Supported, FileName + ' is not a video driver');
    Video := PVideoDriver(P^.Data);
    VideoModeDemo;
    Video := nil;
    AssetUnlock(P);
end;

begin
    Writeln('Begin test');
    AssetIndexSys('DRIVERS', '');
    AssetIndexSys('FONTS', '');
    if ParamCount = 0 then begin
        { Test('EGA386.DRV'); Incomplete, will throw error 200 if attempt use }
        TestDriver('VGA386.DRV');
    end else
        TestDriver(ParamStr(1));
    WriteLn('test complete');
end.