; BSD 3-Clause License
; Copyright (c) 2022, Jerome Shidel
; All rights reserved.

; NASM 2.14rc0 for DOS


%ifdef IdleCPU_Required
	IdleCPU_Proc:
		pushf
		push    ax
		mov     al, [CS:IdleCPU_Method]
		cmp     al, 1
		ja      .NoIdle
		je      .Alternate
		; DOS 5.00+, Windows 3+ -- Release current VM time slice.
		mov     ax, 0x1680
		int     0x2f
		cmp     al, 0x00
		je     .Done
		mov     al, 1
		mov     [CS:IdleCPU_Method], al
	.Alternate:
		hlt
	.NoIdle:
	.Done:
		pop     ax
		popf
		ret
%endif

%ifdef StdOutStr_Required
	StdOutStr_Proc:
		pushm	ax, dx
	.OutLoop:
		mov		dl, [si]
		inc		si
		cmp		dl, 0
		je		.Done
		StdOut  dl
		jmp		.OutLoop
	.Done:
		popm	ax,	dx
		ret
%endif

%ifdef StdOutCRLF_Required
	StdOutCRLF_Proc:
		push	ax
		push	dx
		StdOut	0x0d, 0x0a
		pop		dx
		pop		ax
		ret
%endif

%ifdef StdOutHexWord_Required
	%ifndef StdOutHexByte_Required
		%define StdOutHexByte_Required
	%endif
	StdOutHexWord_Proc:
		push	ax
		mov		al, ah
		call    StdOutHexByte_Proc
		pop		ax
		call    StdOutHexByte_Proc
		ret
%endif

%ifdef StdOutHexByte_Required
	StdOutHexByte_Proc:
		push	cx
		mov		cx, 2
	.Loopy:
		push	cx
		push	ax
		dec		cx
		mov		ax, 4
		mul		cx
		mov		cx, ax
		pop		ax
		push	ax
		shr		ax, cl
		and		ax, 0x0f
		cmp		al, 9
		jle		.Digit
		add		al, 0x57
		jmp		.Show
	.Digit:
		add		al, 0x30
	.Show:
		StdOut	al
		pop		ax
		pop		cx
		loop	.Loopy
		pop		cx
		ret
%endif

%ifdef StdOutIntWord_Required
	StdOutIntWord_Proc:
		pushm		bx, cx, dx
		mov         bx, 0x000a
		mov         cx, 0x0001
	.PrintLoop:
		cmp         ax, bx
		jge         .WayTooBig
		push        ax
	.IsJustRight:
		pop         ax
		add         ax, 0x0030
		push        ax
		push        bx
		push        cx
		StdOut	al
		pop         cx
		pop         bx
		pop         ax
		loop        .IsJustRight
		jmp         .Done
	.WayTooBig:
		inc         cx
		xor         dx, dx
		div         bx
		push        dx
		jmp         .PrintLoop
	.Done:
		popm		bx, cx,	dx
		ret
%endif

%ifdef GetKeyStr_Required
	%idefine GetEnvStr_Required
	GetKeyStr_Proc:
		push	ax
		push	dx
		cld
		jmp		GetEnvStr_Proc.Search
%endif


%ifdef GetEnvStr_Required
	GetEnvStr_Proc:
        push    ax
        push    di
        mov     ax, [cs:PSP_ENV]
        push    ax
        pop     ds
        xor     si, si
        cld
    .Search:
        lodsb
        cmp     al, 0x00
        je      .Failed
        mov     di, bx
    .Repeat:
        mov     ah, [cs:di]
        inc     di
        cmp     ah, al
        jne     .Maybe
        lodsb
        cmp     al, 0x00
        je      .Search
        jmp     .Repeat
    .Maybe:
        cmp     al, '='
        jne     .Skip
        cmp     ah, 0
        je      .Found
    .Skip:
        lodsb
        cmp     al, 0x00
        jne     .Skip
        jmp     .Search
    .Found:
        clc
        jmp     .Done
    .Failed:
    	dec		si
        stc
    .Done:
        pop     di
        pop     ax
        ret
%endif

%ifdef ParseStr_Required
; CS:DI	Point to sub-string handler
; DS:SI Point to asciiz or CR terminated string to processes
; CF set on error return
	ParseStr_Proc:
		push		dx
		mov			dx, 0x2f0d 		; DH = 0x00 or Field Separator Character
									; DL = 0x00 or Additional String Terminator									;  or 0x01 = CR Also Terminates String
	ParseStrAlt_Proc:
		pushm		ax, bx, cx
	.ParseLoop:
		call	    .GetNextParam
		jc			.Finished
		test 		cx, cx
		jz			.Finished

		pushm		si, di, dx
		xchg		bx, si
		call		di	; CX=String Length, SI=Start of String, BX=Start of Next
		popm		si, di, dx
		jc			.Finished

		mov			al,	[si-1]
		test		al, al
		jz			.Finished	; String was null terminated
		cmp			al, dh 		; If was chopped by separator then dec SI
		jne			.ParseLoop
		dec			si
		jmp			.ParseLoop
	.Finished:
		popm		ax, bx, cx
		pop 		dx
		ret

	.GetNextParam:
		cld
		xor			cx, cx
		mov			bx, si
	.GetNextChar:
		lodsb
		test 		al, al		; Null terminated
		jz			.Done
	    cmp         al, dl	    ; is 0x0d when also CR terminated
	    je          .Done
	    cmp			al, dh
	    jne			.NotSeparator
		test		cx, cx
		jnz			.Done

	.NotSeparator:
		cmp			al, 0x20	; Sub-string separator
		jne			.NotSpaceChar
		test		cx, cx		; Ignore empty sub-strings
		jz			.GetNextParam
		jmp			.Done

	.NotSpaceChar:
		test		cx, cx
		jnz 		.NotFirstChar

	    cmp         al, 0x22
    	je          .Quoted
    	cmp         al, 0x27
    	je          .Quoted
    	cmp         al, 0x60
    	je          .Quoted

	.NotFirstChar:
		inc			cx
		jmp			.GetNextChar

	.Quoted:
		mov			ah, al
		inc			bx
	.QuotedLoop:
		lodsb
		cmp			al, ah
		je			.QoutedEnd
		test 		al, al
		jz			.Error		; End of string, no closing quote
	    cmp         al, dl
	    je          .Error		; End of string, no closing quote
		inc			cx
		jmp			.QuotedLoop
	.QoutedEnd:
		jmp			.Done

	.Done:
		clc
		ret
	.Error:
		stc
		ret
%endif

%ifdef StdOutLenStr_Required
	; CX = total bytes
	; DS:SI = String Pointer
	StdOutLenStr_Proc:
		cld
		push		ax
	.DisplayLoop:
		lodsb
		StdOutChar  al
		loop		.DisplayLoop
	.Die:
		pop		    ax
	    ret
%endif

%ifdef LenStrToWordHex_Required
	LenStrToWordHex_Proc:
		xor         ax, ax
		cmp         cx, 0
		je          .DoneError
		cmp         cx, 4
		ja          .DoneError
	.MakeHex:
		push        cx
		mov         cl, 4
		shl         ax, cl
		pop         cx
		mov         bl, [di]
		inc         di
		cmp         bl, 0x30
		jb          .DoneError
		cmp         bl, 0x39
		ja          .NotNumber
		sub         bl, 0x30
		jmp         .Adjusted
	.NotNumber:
		cmp         bl, 0x41
		jl          .DoneError
		cmp         bl, 0x46 ; 5a is Z
		jg          .NotUpper
		sub         bl, 0x37
		jmp         .Adjusted
	.NotUpper:
		cmp         bl, 0x61
		jl          .DoneError
		cmp         bl, 0x66 ; 7a is z
		jg          .DoneError
		sub         bl, 0x57
	.Adjusted:
		xor         bh, bh
		add         ax, bx
		loop        .MakeHex
	.DoneOk:
		clc
	.Done:
		ret
	.DoneError:
		stc
		jmp			.Done
%endif

%ifdef LenStrHexToWord_Required
	LenStrHexToWord_Proc:
		pushm       bx, cx, dx, si, di
		mov			si, di
		call		LenStrToWordHex_Proc
		popm        bx, cx, dx, si, di
		ret
%endif

%ifdef LenStrToWord_Required
	LenStrToWord_Proc:
		xor         ax, ax
		pushm       bx, cx, dx, si, di
		mov			si, di
		add			si, cx
	 .NumLoop:
		cmp         cx, 0
		je          .DoneError
		mov         bl, [di]
		inc         di
		cmp         bl, 'x'
		je          .ForceHex
		cmp         bl, 'X'
		je          .ForceHex

		cmp         bl, 0x41
		jb          .NotUpperCase
		cmp         bl, 0x5a
		ja          .NotUpperCase
		jmp         .IsHex
	.NotUpperCase:
		cmp         bl, 0x61
		jb          .NotLowerCase
		cmp         bl, 0x7a
		ja          .NotLowerCase
		jmp         .IsHex
	.NotLowerCase:
		push        cx
		sub         bl, 0x30
		mov         cx, 10
		mul         cx
		xor         bh, bh
		add         ax, bx
		pop         cx
		loop        .NumLoop
		jmp         .DoneOk
	 .ForceHex:
		pop         bx          ; discard original di
		push		di
		dec         cx
		jmp         .DoAsHex
	 .IsHex:
		pop         di
		push		di
		mov         cx, si
		sub         cx, di
	.DoAsHex:
		call		LenStrToWordHex_Proc
		jmp			.Done
	.DoneOk:
		clc
	.Done:
		popm       bx, cx, dx, si, di
		ret
	.DoneError:
		stc
		jmp			.Done
%endif

; Data and storage required for function calls
%ifdef IdleCPU_Required
	IdleCPU_Method:
		; Initial value = 0, first call if fails, switches to 1 automatically.
		; 0 = DOS 5.00+, Windows 3+ -- Release current VM time slice.
		; 1 = Any DOS, CPU Halt instruction
		; 2 = Do not idle.
		db 	0
%endif

