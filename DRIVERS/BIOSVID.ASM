; BSD 3-Clause License
; Copyright (c) 2021, Jerome Shidel
; All rights reserved.

; NASM 2.14rc0 for DOS

use16

cpu 8086

%include "TPASCAL.INC"

%idefine NotOptimal

%idefine MaxWidth       640
%idefine MaxHeight      480
%idefine MaxColorDepth  8
%idefine MaxColors      256
%idefine MaxProfiles    1
%idefine MemSize        0

%include "DRVVIDS.INC"

Header:
    istruc THeader
        at THeader.Platform,        db 6,'SGEx86'
        at THeader.Class,           db 5,'VIDEO'
        at THeader.Name,            db 10,'BIOS VIDEO'
        at THeader.Version,         dw 1            ; version 1
        at THeader.VersionCompat,   dw 0            ; pre-release devel only
        at THeader.CPU,             dw 0            ; 8086
        at THeader.Flags,           dw 0
        at THeader.MinimumMemory,   dd MemSize
        at THeader.Width,           dw MaxWidth
        at THeader.Height,          dw MaxHeight
        at THeader.ColorDepth,      dw MaxColorDepth
        at THeader.Colors,          dw MaxColors
        at THeader.ColorProfiles,   dw MaxProfiles
    iend

Core:
    istruc TCore ; Absoultely required functions
        at TCore.GetCopyright,      dw GetCopyright
        at TCore.GetLicense,        dw GetLicense
        at TCore.GetModes,          dw GetModes
        at TCore.OpenVideo,         dw OpenVideo
        at TCore.CloseVideo,        dw CloseVideo
        at TCore.SetSync,           dw SetSync
        at TCore.SetBuffered,       dw SetBuffered
        at TCore.UpdateVideo,       dw UpdateVideo
        at TCore.UpdateForce,       dw UpdateForce
        at TCore.GetPalettes,       dw GetPalettes
        at TCore.SetPalettes,       dw SetPalettes
        at TCore.GetProfile,        dw GetProfile
        at TCore.GetPixel,          dw GetPixel
        at TCore.PutPixel,          dw PutPixel
        at TCore.ImageSize,         dw ImageSize
        at TCore.ImageGetPixel,     dw ImageGetPixel
        at TCore.ImagePutPixel,     dw ImagePutPixel
    iend

Performance:
    istruc TPerformance
        ; at TPerformance.FillArea,           dw FillArea
        ; at TPerformance.GetImage,           dw GetImage
        ; at TPerformance.PutImage,           dw PutImage
        ; at TPerformance.PutImageMode,       dw PutImageMode
        ; at TPerformance.PutMaskMode,        dw PutMaskMode
        ; at TPerformance.PutBits,            dw PutBits
        ; at TPerformance.ShiftArea,        dw ShiftArea
    iend

HighLevel:
    istruc THighLevel ; These are provided by the graphics subsystem
    iend

Optional:
    istruc TOptional
        at TOptional.ExtendedData,        dw GetExtendedData ; debugging phase/data
        ; at TPerformance.Region,             dw Region
    iend

    dd  0xffffffff  ; end of functions list

Copyright:
    db  'Copyright (C) 2021, Jerome Shidel',13
    db  'All rights reserved.',0
License:
    db  'BSD 3-Clause License',0

ModeTable: ; Best mode first
    dw  0x0013, 320, 200, 8
    dw  0x0012, 640, 480, 4
    dw  0x0011, 640, 480, 1 ; VGA Monochrome
    dw  0x0010, 640, 350, 4 ; EGA > 64k, 16 color
    dw  0x0010, 640, 350, 2 ; EGA < 64k, 4 color
    dw  0x000f, 640, 350, 1 ; EGA monochrome
    dw  0x000e, 640, 200, 4 ; EGA 16 color
    dw  0x000d, 320, 200, 4 ; EGA 16 color
    dw  0x0006, 640, 200, 1 ; CGA 2 color
    dw  0x0005, 320, 200, 2 ; CGA 4 color, 4 & 5 are the same ?
    ; dw  0x0004, 320, 200, 2 ; CGA 4 color
    dw  0 ; end of mode list

ExtData: ; extended data, can be any size, not used by the Game Engine (except
         ; to do devel debugging). Could be used by a custom driver to pass
         ; non-standard data to an app. recommend first word be size of data
         ; being returned. The GetExtendedData returns a pointer to this
         ; data or nil.
    times 32 dw 0

Syncing:
    dw      0
PrevMode:
    dw      0
;    dw      0x0000,0xa000

PaletteBuffer:
    times MaxColors * 3     db 0

Profile_0:
DefaultPalette:
    %include "VGA.PAL"

%imacro VerticalSync 0
    mov     ax, [cs:Syncing]
    test    ax, ax
    jz      %%NoSync
    mov     dx,  0x03da
    mov     ah, 0x08
%%WaitSyncA:
    in      al, dx
    test    al, ah
    jz      %%WaitSyncA
%%WaitSyncB:
    in      al, dx
    test    al, ah
    jnz     %%WaitSyncB
%%NoSync:
%endmacro

%idefine Width  [Header + THeader.Width]
%idefine Height [Header + THeader.Height]
%idefine Colors [Header + THeader.Colors]
%idefine ColorDepth [Header + THeader.ColorDepth]

%imacro ExportRegisters 0

    mov     [cs:ExtData + 0], ax
    mov     [cs:ExtData + 2], bx
    mov     [cs:ExtData + 4], cx
    mov     [cs:ExtData + 6], dx
    mov     [cs:ExtData + 8], si
    mov     [cs:ExtData + 10], di
    mov     [cs:ExtData + 12], es
    mov     [cs:ExtData + 14], ds

%endmacro

ResetInternals:
    ; Save Current Video Mode
    mov     ah, 0x0f
    int     0x10
    xor     ah, ah
    mov     [PrevMode], ax
    mov     [Syncing], ah
    mov     Colors, word 0x100
    mov     ColorDepth, word MaxColorDepth
    mov     Width, word 640
    mov     Height, word 480
    xor     ax, ax
    mov     [Header + THeader.Font], ax
    mov     [Header + THeader.Font + 2], ax
    mov     [Header + THeader.Monospace], ax
    mov     [Header + THeader.Direction], word 6    ; dmRight - Left to Right
ret

fdriver GetExtendedData, pointer
    push    cs
    pop     dx
    mov     ax, ExtData
edriver


; Core Functions
fpascal GetCopyright, pointer
; function : pointer;
; We just returning a pointer. Declaring as fpascal to avoid PUSH/POP DS
    push    cs
    pop     dx
    mov     ax, Copyright      ; DX:AX
epascal

fpascal GetLicense, pointer
; function : pointer;
; We just returning a pointer. Declaring as fpascal to avoid PUSH/POP DS
    push    cs
    pop     dx
    mov     ax, License      ; DX:AX
epascal

fpascal GetModes, pointer
; function : pointer;
; We just returning a pointer. Declaring as fpascal to avoid PUSH/POP DS
    push    cs
    pop     dx
    mov     ax, ModeTable       ; DX:AX
epascal

fdriver OpenVideo, word, integer
; function (Mode : word) : integer;
    call    ResetInternals
    mov     ax, PARAM_1
    mov     si, ModeTable
.CheckMode:
    mov     bx, [si]
    cmp     bx, 0
    je      .ModeError
    cmp     ax, bx
    je      .ModeOK
    add     si, 8       ; next mode table entry
    jmp     .CheckMode
.ModeOK:
    push    ax
    mov     bx, [si + 2]
    mov     Width, bx
    mov     bx, [si + 4]
    mov     Height, bx
    mov     cx, [si + 6]
    mov     ColorDepth, cx
    mov     ax, 1
    cmp     cl, 1
    jna     .SetColors
    shl     ax, cl
.SetColors:
    mov     Colors, ax
    pop     ax
    int     0x10    ; AH = 0, set video mode
    ExportRegisters


    ; Set Default Color Palette
    push    ds
    pop     es
    mov     cx, Colors
    cmp     cx, 16
    jb      .DontSetPalette
    mov     dx, DefaultPalette
    mov     ax, 0x1012
    xor     bx, bx
    int     0x10
.DontSetPalette:
    ; clear Screen & stage / zero fill

    mov     RESULT, 0   ; No Error
    jmp     .Done
.ModeError:
    mov     RESULT, 1   ; Invalid Function
    jmp     .Done
.MemError:
    mov     RESULT, 8   ; Out of Memory/Not Assigned
    jmp     .Done
.Done:
edriver

pdriver CloseVideo
    mov     ah, 0x0f
    int     0x10
    xor     ah, ah
    mov     bx, [PrevMode]
    cmp     ax, bx
    je      .Done
    ; Reset To Text Mode
    mov     ax, [PrevMode]
    xor     ah, ah
    int     0x10
.Done:
    call    ResetInternals
edriver

pdriver SetSync, boolean
; procedure (Enabled : boolean);
    mov ax, PARAM_1
    xor ah, ah
    mov [cs:Syncing], ax
edriver

pdriver SetBuffered, boolean
; procedure (Enabled : boolean);
; ignored
edriver

pdriver UpdateVideo
; procedure;
; *driver saves/restores previous DS
; Ignored
edriver

pdriver UpdateForce
; Ignored
edriver

pdriver GetPalettes, pointer
; procedure (var Palettes : TPalettes);
    les     dx, PARAM_1
    mov     cx, Colors
    mov     ax, 3
    mul     cx
    mov     cx, ax
    push    cx
    mov     ax, 0x1017
    xor     bx, bx
    mov     cx, Colors
    int     0x10
    ; multiply palette values by 4
    cld
    pop     cx
    mov     di, dx
    add     dx, cx
    mov     cl, 2
.UpConvert:
    mov     al, [es:di]
    shl     ax, cl
    stosb
    cmp     di, dx
    jne     .UpConvert
edriver

pdriver SetPalettes, pointer
; procedure (var Palettes : TPalettes);
    ; copy then divide palette values by 2
    mov     cx, Colors
    push    cx
    mov     ax, 3
    mul     cx
    mov     cx, ax
    cld
    lds     si, PARAM_1
    push    ds
    pop     es
    mov     di, PaletteBuffer
    push    di
    mov     dx, di
    add     dx, cx
    mov     cl, 2
.DownConvert:
    lodsb
    shr     ax, cl
    stosb
    cmp     di, dx
    jne     .DownConvert
    VerticalSync
    pop     dx ; was di
    pop     cx
    mov     ax, 0x1012
    xor     bx, bx
    int     0x10
edriver

fdriver GetProfile, word, pointer, boolean
; function (Profile : word; var Palettes : TPalettes): boolean;
; We just returning a pointer. Declaring as fpascal to avoid PUSH/POP DS
    mov     ax, PARAM_1
    cmp     ax, 1
    ja      .DoesNotExist ; 0=default, 1=profile 0, only one profile for now
    ; copy default palette
    cld
    les     di, PARAM_2
    mov     si, Profile_0

    mov     cx, MaxColors * 3    ; MaxColors * 3 / 4
    push    di
    rep     movsb
    mov     dx, di
    pop     di
    ; UpConvert
    mov     cl, 2
.UpConvert:
    mov     al, [es:di]
    shl     ax, cl
    stosb
    cmp     di, dx
    jne     .UpConvert
    mov     RESULT, TRUE
    jmp     .Done
.DoesNotExist:
    mov     RESULT, FALSE
.Done:
edriver

fdriver GetPixel, integer, integer, word
; function (x, y : integer) : word;
    mov     ax, Height
    mov     dx, PARAM_2
    cmp     dx, ax
    jnb     .NoPixel
    mov     ax, Width
    mov     cx, PARAM_1
    cmp     cx, ax
    jnb     .NoPixel
    mov     ah, 0x0d
    xor     bh, bh
    int     0x10
    jmp     .Done
.NoPixel:
    xor     al, al
.Done:
edriver

pdriver PutPixel, integer, integer, word
; procedure (x, y : integer; Color : TColor);
    mov     ax, Height
    mov     dx, PARAM_2
    cmp     dx, ax
    jnb     .NoDraw
    mov     ax, Width
    mov     cx, PARAM_1
    cmp     cx, ax
    jnb     .NoDraw
    mov     ah, 0x0c
    xor     bh, bh
    mov     al, PARAM_3
    int     0x10
.NoDraw:
edriver

; Basic Image Functions Required
fdriver ImageSize, word, word, word
; function (Width, Height : word) : word;
    mov     ax, PARAM_1
    mov     cx, PARAM_2
    mul     cx
    cmp     dx, 0
    jne     .TooBig
    mov     cx, ax
    and     cx, 0x0003
    jz      .EvenFour   ;
    and     ax, 0xfffc
    add     ax, 4
    jo      .TooBig
.EvenFour:
    add     ax, ImageHeadSize  ; add size of image control header
    jno     .Done
.TooBig:
    mov     ax, 0
.Done:
edriver

%imacro ImageHeader 2
; out ax=byte width, bx=image width, dx=image height, cx=data byte size
    les     di, %2
    %ifidni %1, var
        mov     bx, [es:di + 2]
        push    bx
        mov     di, [es:di]
        pop     es
    %endif
    clc
    mov     al, [es:di+6] ; compression
    cmp     al, 0       ; 0=uncompressed
    je      .Uncompressed
    cmp     al, 255     ; 255=not compressible
    je      .Uncompressed
    stc
.Uncompressed:
    pushf
    mov     bx, [es:di] ; width
    mov     dx, [es:di+2] ; height
    mov     ax, [es:di+4] ; LineWidth
    mov     cx, [es:di+8] ; DataSize
    add     di, ImageHeadSize    ; point to first pixel
    popf
%endmacro

fdriver ImageGetPixel, pointer, integer, integer, word
; function (Image : PImage; X, Y : integer) : word;
    ImageHeader pointer, PARAM_1
    jc      .Done
    mov     bx, PARAM_2 ; x
    mov     cx, PARAM_3 ; y
    cmp     bx, ax
    jnb     .Outside
    cmp     cx, dx
    jnb     .Outside
    mul     cx
    add     bx, ax
    xor     ah, ah
    mov     al, [es:di+bx]
    jmp     .Done
.Outside:
    xor     ax, ax
.Done:
edriver

pdriver ImagePutPixel, pointer, integer, integer, word
; procedure (var Image : PImage; X, Y : integer; Color : word);
    ImageHeader var, PARAM_1
    jc      .Done
    mov     bx, PARAM_2 ; x
    mov     cx, PARAM_3 ; y
    cmp     bx, ax
    jnb     .Done    ; outside image
    cmp     cx, dx
    jnb     .Done    ; Outside image
    mul     cx
    add     bx, ax
    mov     ax, PARAM_4
    mov     [es:di+bx], al
.Done:
edriver

%imacro RegionAdjust 4
    xor     bx, bx
    mov     ax, Width
    cmp     %1, ax
    jnb     %%NoDraw
    jna     %%StartXOK
    mov     %1, bx
%%StartXOK:
    mov     ax, Width
    cmp     %3, ax
    jb      %%EndXOK
    jge     %%EndXAdjust
    jmp     %%NoDraw
%%EndXAdjust:
    dec     ax
    mov     %3, ax
%%EndXOK:
    mov     ax, Height
    cmp     %2, ax
    jnb     %%NoDraw
    jna     %%StartYOK
    mov     %2, bx
%%StartYOK:
    mov     ax, Height
    cmp     %4, ax
    jb      %%EndYOK
    jge     %%EndYAdjust
    jmp     %%NoDraw
%%EndYAdjust:
    dec     ax
    mov     %4, ax
%%EndYOK:
    mov     cx, %1
    cmp     cx, %3
    ja      %%NoDraw
    mov     dx, %2
    cmp     dx, %4
    ja      %%NoDraw
    clc
    jmp     %%Done
%%NoDraw:
    stc
%%Done:
%endmacro

; Performance related functions to help a little
pdriver Region, integer, integer, integer, integer, word
; procedure (x1, y1, x2, y2 : integer; Color : TColor);

    RegionAdjust PARAM_1, PARAM_2, PARAM_3, PARAM_4
    jc      .NoDraw

    mov     ah, 0x0c
    xor     bh, bh
    mov     al, PARAM_5

.DrawLoop:
    int     0x10
    cmp     cx, PARAM_3
    je      .NextRow
    inc     cx
    jmp     .DrawLoop
.NextRow:
    cmp     dx, PARAM_4
    je      .NoDraw
    mov     cx, PARAM_1
    inc     dx
    jmp     .DrawLoop
.NoDraw:

edriver



