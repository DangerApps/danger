#!/bin/bash

for i in *.DRV ; {
    if [[ "${i}" != '*.DRV' ]] ; then
        rm "${i}" || exit 1
    fi;
}

for i in *.ASM ; {
    if [[ "${i}" != '*.ASM' ]] ; then
        nasm "${i}" -I../ -fbin -O9 -o "${i%.*}.DRV" || exit 1
        [[ ! -e "${i%.*}.DRV" ]] && exit 1
    fi;
}

ls -l *.DRV