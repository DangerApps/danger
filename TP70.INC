; Copyright (C) 2021 Jerome Shidel
; BSD 3-Clause License

; NASM 2.14rc0 for DOS

%idefine TURBOPASCAL 7.0
%idefine TP70
%undef   TP55

%define PASCAL_DATA _DATA
%define PASCAL_SHARED _BSS
%define PASCAL_CODE _TEXT
